
const logger = require("winston")
const exactMath = require('exact-math');

class Trade {
    constructor({ buyPrice, quantity, buyOrder }) {
        this.buyPrice = buyPrice
        this.quantity = quantity
        this.buyOrder = buyOrder
        this.buyTime = new Date()
        this.sellPrice = null
        this.sellTime = null
        this.sellOrder = null
    }

    setData(data = {}) {
        Object.assign(this, data)
    }

    calculateProfit() {
        let totalBuyAmount = exactMath.add(exactMath.mul(this.buyOrder.totalQuantity, this.buyOrder.avgPrice), this.buyOrder.feeAmount)
        let amountFromSell = exactMath.sub(exactMath.mul(this.sellOrder.totalQuantity, this.sellOrder.avgPrice), this.sellOrder.feeAmount)

        let profitInINR = exactMath.sub(amountFromSell, totalBuyAmount)
        let profitPercent = exactMath.round(exactMath.mul(exactMath.div(profitInINR, totalBuyAmount), 100), -2)
        let totalTradingFees = exactMath.add(this.buyOrder.feeAmount, this.sellOrder.feeAmount)
        return { profitInINR, profitPercent, totalTradingFees, totalBuyAmount, amountFromSell }
    }
}

module.exports = Trade

