const exactMath = require('exact-math')
const logger = require('winston')

module.exports = {
  /*
  either gridWidth or noOfGrids. Preference in the same order
*/
  calculateGrids({ start, end, gridWidth, noOfGrids }) {
    let grids = []
    if (gridWidth > 0) {
      for (; start <= end; start += gridWidth)
        grids.push(exactMath.round(start, -2));
    }
    else if (noOfGrids > 0) {
      const gridSize = (end - start) / noOfGrids;
      for (; noOfGrids > 0; noOfGrids--) {
        grids.push(start);
        start = exactMath.round(start + gridSize, -2);
      }
    }
    return grids;
  },

  async wait(ms) {
    await new Promise((res, rej) => setTimeout(res, ms))
  },

  retryOnException(func, multiplier, { maxWait = 10000, maxRetries = 5, resultProcesser } = {}) {
    return async function (...args) {
      let curRetryCount = 0;
      while(true) {
        try {
          let result =  await func.apply(this, args)
          if(resultProcesser)
            result =  resultProcesser.call(this, result)
          return result
        } catch (ex) {
          // if 429 sleep for 1m no matter what
          if(ex.statusCode == 429) 
            await new Promise(res => setTimeout(res, 60000))

          if (curRetryCount > maxRetries) {
            logger.error(`${func.name} threw exception`, ex)
            throw ex;
          }
          curRetryCount++;
          logger.error(`[RetryOnException] Will be retrying ${func.name} .`, { functionName: func.name, curRetryCount, maxRetries, maxWait, ex })
          await new Promise((res) => setTimeout(res, Math.min(multiplier * curRetryCount, maxWait)))
        }
      }
     
    }
  }
}