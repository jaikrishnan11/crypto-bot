
const telegramBot = require('../clients/telegram-client')
const CONFIG = require('config').get('longShortBot')
const exactMath = require('exact-math')
const {calculateGrids, wait } =require('./utils')
const logger = require('winston')
const Events = require('../exchange/Events')
const _ = require('lodash')

class ShortBot {
  constructor(binance) {
    this.binance = binance
    this.state = {}
    this.init()
  }

  async init(){
    await this.binance.init()
    await this.attachListeners()
    await this.constructState()
    setTimeout(this.processMovingGrid.bind(this),  60 * 60 * 1000)
  }

  async attachListeners(){
    this.binance.on('BUY_FILLED', this.onBuyFilled.bind(this))
    this.binance.on('SELL_FILLED', this.onSellFilled.bind(this))
  }

  async constructState() {
    if(CONFIG.restoreState) {

    }
    else {
      this.state = {
        gridDetails: {
          start: CONFIG.gridDetails.start,
          end: CONFIG.gridDetails.end
        },
        grids: calculateGrids(CONFIG.gridDetails),
        noOfTradesSoFar: 0,
        gridToTradeMap: {},
        totalRealisedpnl: 0,
        totalCommission: 0,
        lastReadUserTradeTime: new Date().getTime()
      }
      await this.initializeGrids()
      await this.processUnProcessedTrades()
    }
  }

  async initializeGrids() {
    this.state.eachGridMoney = exactMath.round(exactMath.div(CONFIG.initialAmount, this.state.grids.length), -2);
    for(const grid of this.state.grids.reverse()) {
      let quantity = exactMath.round(this.state.eachGridMoney / grid, -3)
      this.state.gridToTradeMap[grid] = new Trade(null, null, {price: grid, qty: quantity})
      //let response = await this.binance.getShortContract(quantity, grid)
      //this.state.gridToTradeMap[response.price] = new Trade(response.clientOrderId, response)
    }
  }

  async processUnProcessedTrades() {
    let unProcessedTrades = []
    let ticker = await this.binance.futureMarkPrice("ETHUSDT")
    let curPrice = parseFloat(ticker.markPrice);
    for(let grid of Object.keys(this.state.gridToTradeMap)){
      let trade  = this.state.gridToTradeMap[grid]
      if(trade.status == Events.ORDER_NOT_CREATED)
        unProcessedTrades.push(grid)
    }
    if(unProcessedTrades.length == 0){
      logger.info(`No UnProcessed trades left`)
      return
    }

    for(let grid of unProcessedTrades){

      let trade = this.state.gridToTradeMap[grid]
      if(trade.price < curPrice - CONFIG.stoplimitThreshold)
        continue;
      let buyOrder = await this.binance.getShortContract(trade.qty, trade.price)
      if(buyOrder.code && buyOrder.code < 0){
        logger.error(`[ShortBot] buyorder failed`, buyOrder)
        throw 'Some error placing order'
      }
      logger.info(`[ShortBot] Sell order for ${buyOrder.price}`)
      delete this.state.gridToTradeMap[grid]
      this.state.gridToTradeMap[buyOrder.price] = new Trade(buyOrder.clientOrderId, buyOrder)
    }
    setTimeout(this.processUnProcessedTrades.bind(this), 1000)
  }

  async onSellFilled(updatedSellOrder){
    if(!this.state.gridToTradeMap[updatedSellOrder.originalPrice]){
      logger.warn(`Order not found`, updatedSellOrder)
      return
    }
    let retryCount = 0;
    while(this.state.gridToTradeMap[updatedSellOrder.originalPrice].status == Events.ORDER_NOT_CREATED){
      await wait(500)
      retryCount++;
      if(retryCount > 100){
        telegramBot.sendErrorMessage(`[ShortBot] onSellFilled retry failed`);
        logger.error(`[ShortBot] onSellFilled retry failed`, updatedSellOrder)
        return
      }
    }
    logger.info(`[ShortBot] Sell order filled ${updatedSellOrder.originalPrice}`, updatedSellOrder)
    let trade = this.state.gridToTradeMap[updatedSellOrder.originalPrice];
    let buyOrder = Object.assign(this.state.gridToTradeMap[updatedSellOrder.originalPrice].buyOrder, updatedSellOrder)
    
    const buyPrice = parseFloat(buyOrder.originalPrice)
    const buyQty = parseFloat(buyOrder.origQty)
    if(buyPrice == NaN){
      logger.error(`SellPrice parse error`)
      throw `SellPrice parse error`
    }

    let sellOrder = await this.binance.releaseShortContract(buyQty, buyPrice - CONFIG.gridDetails.sellVariationInUnit)
    trade.sellOrder = sellOrder
    this.state.gridToTradeMap[sellOrder.clientOrderId] = trade
  }

  async onBuyFilled(updatedBuyOrder){
    if(!this.state.gridToTradeMap[updatedBuyOrder.clientOrderId]){
      logger.warn(`[ShortBot] Buy Order not found`, updatedBuyOrder)
      return
    }
    let trade = this.state.gridToTradeMap[updatedBuyOrder.clientOrderId]
    Object.assign(trade.sellOrder, updatedBuyOrder)
    let buyOrder = trade.buyOrder
    const buyPrice = parseFloat(buyOrder.originalPrice)
    const buyQty = parseFloat(buyOrder.origQty)
    this.state.noOfTradesSoFar++;

    let profitDetails = trade.getProfitDetails()
    this.state.totalRealisedpnl += profitDetails.realisedpnl;
    this.state.totalCommission += profitDetails.commission;
    logger.info(`[ShortBot] TradeCompleted @${trade.buyOrder.originalPrice}. soFar: ${this.state.noOfTradesSoFar}`, {trade, profitDetails, totalRealisedpnl: this.state.totalRealisedpnl, commission: this.state.totalCommission})
    await telegramBot.sendInfoMessage(`[ShortBot] Trade Completed @${buyPrice} soFar: ${this.state.noOfTradesSoFar}, totalPNL: ${this.state.totalRealisedpnl} totalComission: ${this.state.totalCommission} ${JSON.stringify(profitDetails)}`)
    
    //TODO : Delete buy order id
    buyOrder  = await this.binance.getShortContract(buyQty, buyPrice)
    this.state.gridToTradeMap[buyOrder.price] = new Trade(buyOrder.clientOrderId, buyOrder)
    logger.info(`[ShortBot] Sell Order placed @${buyPrice}`)
  }

  async moveGrid(doesGridNeedToBeMoved, generateNewGrids) {
    let gridsToBeMoved = _.filter(this.state.grids, doesGridNeedToBeMoved);
    
    let ethToBeSoldAtLoss = 0.0; // Is this the right term for shortBot ?
    logger.info(`Going to move grids`, {gridsToBeMoved})
    for(let grid of gridsToBeMoved){
      let trade = this.state.gridToTradeMap[grid];
      let orderToCancel = trade?.sellOrder?.orderId || trade?.buyOrder?.orderId
      ethToBeSoldAtLoss += (trade?.sellOrder ? trade.qty : 0.0)
      if(orderToCancel) {
        // What if order filled before cancelling. EDGE CASE
        let result = await this.binance.cancelOrderId(orderToCancel)
        logger.info(`Order @${grid} cancelled`, {result})
      }
      delete this.state.gridToTradeMap[grid]
      delete this.state.gridToTradeMap[trade?.sellOrder?.clientOrderId]
    }

    if(ethToBeSoldAtLoss > 0.0) {
      let buyOrder = await this.binance.releaseShortContract(ethToBeSoldAtLoss);
      logger.info(`Market Buy order completed`, {buyOrder, ethToBeSoldAtLoss})
      telegramBot.sendInfoMessage(`[ShortBot] Sold ${ethToBeSoldAtLoss}  at loss :( `)
    }

    let newGrids = _.filter(this.state.grids, grid => !doesGridNeedToBeMoved(grid))
    generateNewGrids(gridsToBeMoved.length).forEach(newGrid => {
      newGrid = exactMath.round(newGrid, -2);
      let quantity = exactMath.round(exactMath.div(this.state.eachGridMoney, newGrid), -3)
      this.state.gridToTradeMap[newGrid] = new Trade(null, null, {price: newGrid, qty: quantity})
      newGrids.push(newGrid)
    })

    Object.assign(this.state.gridDetails, {start: _.min(newGrids), end: _.max(newGrids)})
    this.state.grids = _.sortBy(newGrids)
  }

  async processMovingGrid() {
    let ticker = await this.binance.futureMarkPrice("ETHUSDT")
    let curPrice = 9 || parseFloat(ticker.markPrice);
    let leftMostGrid = this.state.gridDetails.start;
    let rightmostGrid = this.state.gridDetails.end;
    let totalWidth = rightmostGrid - leftMostGrid;
    let gridWidth = Math.abs(this.state.grids[0] - this.state.grids[1])

    if(curPrice + 20 < leftMostGrid) {
      logger.info(`[ShortBot] Moving grid from right to left`)
      telegramBot.sendInfoMessage(`[LongBot] Moving grid from right to left. a.k.a moving with loss`)
      const threshold = rightmostGrid - totalWidth * (CONFIG.movingGridPercentage / 100);
      const doesGridNeedToBeMoved = grid =>  grid >= threshold
      const generateNewGrids = noOfNewGrids => _.range(leftMostGrid - gridWidth, leftMostGrid - gridWidth * noOfNewGrids - 1, gridWidth * - 1)
      await this.moveGrid(doesGridNeedToBeMoved, generateNewGrids)
    }
    else if(curPrice - 20 > rightmostGrid) {
      logger.info(`[ShortBot] Moving grid from left to right :(`)
      telegramBot.sendInfoMessage((`[ShortBot] Moving grid from left to right.`))
      const threshold = leftMostGrid + totalWidth * (CONFIG.movingGridPercentage / 100);
      const doesGridNeedToBeMoved = grid =>  grid <= threshold
      const generateNewGrids = noOfNewGrids => _.range(rightmostGrid + gridWidth, rightmostGrid +  gridWidth * noOfNewGrids + 1, gridWidth )
      await this.moveGrid(doesGridNeedToBeMoved, generateNewGrids)
    }
    else {
      logger.debug(`[ShortBot] Bot within range. All is Well :)`)
    }
    setTimeout(this.moveGrid.bind(this), CONFIG.movingGridCheckTime * 60 * 1000)
  }

}

class Trade {
  constructor(id, buyOrder, data={}){
    this.id = id;
    this.buyOrder = buyOrder
    if(buyOrder){
      this.price = parseFloat(buyOrder.price)
      this.qty = parseFloat(buyOrder.origQty)
    }
    this.status = buyOrder ? Events.ORDER_CREATED : Events.ORDER_NOT_CREATED 
    Object.assign(this, data)
  }

  getProfitDetails() {
    return {
      realisedpnl: parseFloat(this.sellOrder.realizedProfit),
      commission: parseFloat(this.sellOrder.commission)
    }
  }
}

module.exports = ShortBot