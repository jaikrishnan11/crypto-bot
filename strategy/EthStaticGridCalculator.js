const exactMath = require('exact-math');
const _ = require("lodash");

const TRADING_FEES = 0.1;

class EthStaticGrid {
    constructor() {
        this.noOfGrids = 75;
        this.minGrid = 3900;
        this.maxGrid = 5100;
        this.triggerValue = 4500;
        this.amountToBeInvested = 5000;
        this.shortInvestment = this.longInvestment = this.calculateLongOrShortInvestment();
        this.valueForEachGrid = exactMath.div(this.amountToBeInvested, this.noOfGrids);
        this.shortGrids = this.computeShortGridValues();
        this.longGrids = this.computeLongGridValues();
        console.log("long grid length", _.size(_.keys(this.longGrids)));
        console.log("short grid length", _.size(_.keys(this.shortGrids)));
    }

    calculateLongOrShortInvestment() {
        return exactMath.div(this.amountToBeInvested, 2);
    }

    computeLongGridValues() {
        let longGrids = {};
        let variationValue = exactMath.div(exactMath.sub(this.maxGrid, this.minGrid), this.noOfGrids);
        if (variationValue < 10) {
            throw new Error("this variation will end up in loss", variationValue)
        }
        for (let i = this.minGrid; i < this.maxGrid; i = i + variationValue) {
            longGrids[i] = i + variationValue;
        }
        console.log(longGrids);
        return longGrids;
    }

    computeShortGridValues() {
        let shortGrids = {};
        let variationValue = exactMath.div(exactMath.sub(this.maxGrid, this.minGrid), this.noOfGrids);
        for (let i = this.minGrid + variationValue; i <= this.maxGrid; i = i + variationValue) {
            shortGrids[i] = i - variationValue;
        }
        console.log(shortGrids);
        return shortGrids;
    }

    calculateProfitInTrade(buyPrice, sellPrice, price) {
        let quantity = exactMath.round(exactMath.div(price, buyPrice), -4);
        // console.log("quantity", quantity);
        let totalBuyAmount = exactMath.mul(quantity, buyPrice);
        totalBuyAmount = totalBuyAmount - exactMath.mul(totalBuyAmount, 0.001)
        // console.log("totalBuyAmount ", totalBuyAmount);
        let amountFromSell = exactMath.mul(quantity, sellPrice);
        amountFromSell = amountFromSell - exactMath.mul(amountFromSell, 0.001)
        // console.log("amountFromSell ", amountFromSell);
        let profitInINR = exactMath.sub(amountFromSell, totalBuyAmount);
        // console.log(profitInINR);
        return profitInINR;
    }

    calculateWarmUpDetails() {
        let longProfit = 0;
        for (let i = this.triggerValue; i <= this.maxGrid; i++) {
            if (this.longGrids[i.toString()] > 0) {
                // console.log("buy ", i, "sell ", this.longGrids[i.toString()])
                longProfit = longProfit + this.calculateProfitInTrade(i, this.longGrids[i.toString()], this.valueForEachGrid);
            }
        }
        console.log("long profit", exactMath.round(longProfit, -2));
        let shortProfit = 0;
        for (let i = this.triggerValue; i <= this.maxGrid; i++) {
            if (this.shortGrids[i.toString()] > 0) {
                // console.log("buy ", i, "sell ", this.shortGrids[i.toString()])
                shortProfit = shortProfit + this.calculateProfitInTrade(this.maxGrid, i, this.valueForEachGrid);
            }
        }
        console.log("shortProfit", exactMath.round(shortProfit, -2));
        let overallLoss = exactMath.round(shortProfit + longProfit, -2)
        console.log("over all loss", overallLoss);
        console.log("loss percentage", exactMath.round(exactMath.div(overallLoss * -1, this.amountToBeInvested), -5) * 100);
        let singleProfitValue = this.calculateProfitInTrade(4000, 4016,this.valueForEachGrid);
        console.log("profit for one trade", singleProfitValue);
        console.log("Monthly profit", ((6000 * singleProfitValue) / this.amountToBeInvested) * 100);
        console.log("Trades required for warm up[long + short]", overallLoss * -1 / this.calculateProfitInTrade(3900, 3916, this.valueForEachGrid))
    }

    test() {
        console.log("working");
    }
}

// new EthStaticGrid().calculateProfitInTrade(100, 50, 100);
new EthStaticGrid().calculateWarmUpDetails();