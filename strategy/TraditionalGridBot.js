
const logger = require("winston")
const Events = require('../exchange/Events')
const DefaultConfig = require('config').get('traditionalGridBot')
const exactMath = require('exact-math');
const telegramBot = require('../clients/telegram-client')
const fs = require('fs')

const Trade = require('./Trade.js');

class TraditionalGridBot {
    constructor(exchange, esClient, opts = {}) {
        this.esClient = esClient;
        this.options = Object.assign({}, DefaultConfig, opts)
        Object.assign(this, this.options)
        this.exchange = exchange;
        this.registerEventListeners()
        this.tradeMap = new Map()
        this.currentTotalProfit = 0.0;
        this.noOfTradesDone = {};
        this.initializeGrid()
    }

    async initializeGrid() {
        logger.info("Fetching config from ES");
        const config = await this.esClient.getConfig("forex-usdt-inr", "static-default");
        logger.info("Fetched config from ES", config);
        Object.assign(this, config);
        await this.exchange.startOrderStatusPoller()
        if (require('config').get('restoreState') && fs.existsSync(this.options.stateFilePath)) {
            await this.restoreState()
            await this.sleep(1)
            return;
        }
        this.noOfGrids = Math.floor(exactMath.div(exactMath.sub(this.gridEnd, this.gridStart), this.gridInterval)) + 1
        this.gridAmount = Math.floor(this.initialAmount / this.noOfGrids)
        logger.info("Initialising grid", { noOfGrids: this.noOfGrids, gridAmount: this.gridAmount })

        for (let startGrid = this.options.gridStart; startGrid <= this.options.gridEnd; startGrid += this.options.gridInterval) {
            let buyQuantity = exactMath.round(exactMath.div(this.gridAmount, startGrid), -2)
            startGrid = exactMath.round(startGrid, -2)
            let buyOrder = await this.exchange.placeOrder('buy', { type: 'buy', limit: startGrid, quantity: buyQuantity })
            this.tradeMap.set(buyOrder.id, new Trade({ buyPrice: startGrid, buyOrder, quantity: buyQuantity }))
            await this.sleep(1.5)
        }
        logger.info("Initial buys orders placed :)")
    }

    saveState() {
        logger.info(`Saved state(TraditionalGridBot)`)
        let state = {
            noOfGrid: this.noOfGrids,
            gridAmount: this.gridAmount,
            tradeMap: Array.from(this.tradeMap.entries()),
            currentTotalProfit: this.currentTotalProfit,
            noOfTradesDone: this.noOfTradesDone
        }
        fs.writeFileSync(this.options.stateFilePath, JSON.stringify(state))
    }

    async restoreState() {
        let state = JSON.parse(fs.readFileSync(this.options.stateFilePath))
        logger.info(`Restoring state. Finges crossed !!`, state)
        Object.assign(this, state)
        this.tradeMap = new Map(state.tradeMap)
        this.tradeMap = new Map([...this.tradeMap].map(([id, trade]) => {
            return [id, Object.assign(new Trade({}), trade)]
        }))
            ;
    }

    registerEventListeners() {
        //  this.exchange.on(Events.TICK, this.processTick.bind(this))
        this.exchange.on(Events.BUY_ORDER_FILLED, this.processBuyOrderFillEvent.bind(this))
        this.exchange.on(Events.SELL_ORDER_FILLED, this.processSellOrderFillEvent.bind(this))

    }

    async processBuyOrderFillEvent(event) {

        let buyOrderId = event.id
        if (!this.tradeMap.has(buyOrderId)) {
            logger.warn("Buy order not found", { buyOrderId })
            return
        }
        let trade = this.tradeMap.get(buyOrderId)
        Object.assign(trade.buyOrder, event)
        logger.info(`Buy order filled for grid ${trade.buyPrice}`, { grid: trade.buyPrice })
        let sellPrice = exactMath.round(trade.buyPrice + this.sellVariation, -2)
        let sellOrder = await this.exchange.placeOrder('sell', { limit: sellPrice, quantity: trade.buyOrder.totalQuantity })
        trade.setData({ sellPrice, sellTime: new Date() })
        trade.sellOrder = sellOrder
        this.tradeMap.delete(trade.id);
        this.tradeMap.set(sellOrder.id, trade)
    }

    async processSellOrderFillEvent(event) {
        let sellOrderId = event.id

        let trade = this.tradeMap.get(sellOrderId)
        if (!this.tradeMap.has(sellOrderId)) {
            logger.warn("Sell Order not found", { sellOrderId })
            telegramBot.sendInfoMessage(`Sell Order completed but not found in our bot`)
            return
        }
        Object.assign(trade.sellOrder, event)
        logger.info(`Sell order filled for grid ${trade.buyPrice}`, { grid: trade.buyPrice })
        this.noOfTradesDone[trade.buyPrice] = (this.noOfTradesDone[trade.buyPrice] || 0) + 1;

        // Profit Calculation
        const profitDetails = trade.calculateProfit()
        this.currentTotalProfit += profitDetails.profitInINR

        logger.info(`[StaticBot] Trade complete @${trade.buyPrice}`, { noOfTrades: this.noOfTradesDone, profitDetails, curTotalProfit: this.currentTotalProfit })
        telegramBot.sendInfoMessage(`[StaticBot] Trade complete @${trade.buyPrice}. noOfTrades: ${JSON.stringify(this.noOfTradesDone)}, currentTradeProfit: ${profitDetails.profitInINR}`)
        await this.sleep(5)
        let newBuyOrder = await this.exchange.placeOrder('buy', { limit: trade.buyPrice, quantity: event.totalQuantity })
        logger.info(`New Buy order placed`, newBuyOrder)
        this.tradeMap.set(newBuyOrder.id, new Trade({ buyPrice: trade.buyPrice, buyOrderId: newBuyOrder.id, quantity: trade.quantity, buyOrder: newBuyOrder }))

        this.tradeMap.delete(sellOrderId)
    }

    async processTick(tick) {
        console.log(tick)
    }

    async sleep(timeInSeconds) {
        return new Promise(res => setTimeout(res, timeInSeconds * 1000))
    }
}

module.exports = TraditionalGridBot