const logger = require("winston")
const Events = require('../exchange/Events')
const FILE_CONFIG = require('config').movingGridBot // Nothing here
const exactMath = require('exact-math');
const _ = require('lodash');
const Heap = require('heap');
const telegramBot = require('../clients/telegram-client')
const fs = require('fs')

/*
* Start after some time. 
* Trade map persist for recovery
* Config at individual grid level
*/

class MovingGridBot {
    constructor(exchange, esClient, opts = {}) {
        this.options = Object.assign({ initNow: true }, FILE_CONFIG, opts)
        this.exchange = exchange;
        this.esClient = esClient;
        this.tradeMap = new Map()
        this.gridToTradeMap = new Map()
        this.curHotGrids = new Set()
        this.gridConfig = {}
        this.totalTradesSinceStart = 0;
        if (this.options.initNow)
            this.initialize().catch(err => {
                logger.error(`Initialize failed`, err)
                throw err
            })
    }

    async initialize() {
        await this.esClient.verifyElasticClient()
        if (require('config').get('restoreState') && fs.existsSync(this.options.stateFilePath)) {
            await this.restoreState()
            this.exchange.startOrderStatusPoller()
            // Wait for the order Status poller to update current orders
            await new Promise(res => setTimeout(res, 10000))
        }
        else {
            await this.refreshGridConfig()
            logger.debug("Got the grid data", this.gridConfig)
            this.remainingMoney = this.gridConfig.overall_amount
            this.curWorth = this.gridConfig.overall_amount
            let [curHotGrids, curHotGridsAll] = await this.getCurHotGrids()
            this.curHotGrids = curHotGrids
            this.curHotGridsAll = curHotGridsAll
            this.exchange.startOrderStatusPoller()
        }

        await this.periodicGridAndConfigUpdater()
        this.registerEventListeners()
        this.attachCommandListeners()
    }

    saveState() {
        logger.info(`Saved state(MovingGridBot)`)
        let state = {
            gridToTradeMap: Array.from(this.gridToTradeMap.entries()),
            curWorth: this.curWorth,
            version: "0.0",
            timestamp: new Date().toLocaleString()
        }
        fs.writeFileSync(this.options.stateFilePath, JSON.stringify(state))
    }

    /**
     *  We want to store as less variables as possible since we can cover more blast area. 
     *  If the no. of variables saved is high, we have overhead of maintaining the consistency of those variables when program crashes
     *  
     *  When (re)starting overall_amount is assumed to include the profits so far   
     */
    async restoreState() {
        await this.refreshGridConfig()
        let state = JSON.parse(fs.readFileSync(this.options.stateFilePath))
        logger.info(`Restoring state. Finges crossed !!`, state)

        if(state.version != "0.0")
            throw `InCompatible version found. Expected: 0.0 Actual: ${state.version}`

        this.curWorth = state.curWorth
        this.gridToTradeMap = new Map(state.gridToTradeMap)
        let totalAmountBorrowed = 0.0;
        [...this.gridToTradeMap.values()].forEach(trade => {
            totalAmountBorrowed += trade.amountBorrowed
            this.tradeMap.set(trade.buyOrder.id, trade);

            if (trade.sellOrder)
                this.tradeMap.set(trade.sellOrder.id, trade);
        })

        if (exactMath.sub(this.curWorth, totalAmountBorrowed) < 0) {
            logger.error(`Amount borrowed(${totalAmountBorrowed}) is > curWorth(${this.curWorth}). Correct something !`);
            throw "Amount borrowed is > curWorth";
        }

        this.curHotGrids = new Set(this.gridToTradeMap.keys())
        this.curHotGridsAll = new Set(this.gridToTradeMap.keys())

        // Need to reduce the remaining_money based on the outstanding orders
        this.remainingMoney = exactMath.sub(this.curWorth, totalAmountBorrowed)
        logger.info(`Restored state`, this.getCurMoneyState())
        telegramBot.sendInfoMessage(`Restored state : ${JSON.stringify(this.getCurMoneyState(), null, 2)}`)

    }

    attachCommandListeners() {
        telegramBot.attachCommandListener(Events.TelegramCommand.STATUS, async () => {
            let curTicker = await this.exchange.getCurTicker();
            let curPrice = parseFloat(curTicker.lastPrice)
            let openBuyOrders = new Set(), openSellOrders = new Set(), partialBuy = new Set()
            this.tradeMap.forEach((trade) => {
                if (trade.sellOrder)
                    openSellOrders.add(trade.sellOrder.pricePerUnit)
                else {
                    openBuyOrders.add(trade.buyOrder.pricePerUnit);
                    if(trade.buyOrder.status == "partially_filled")
                    partialBuy.add(trade.buyOrder.pricePerUnit);
                }
            })
            partialBuy = [...partialBuy]
            if(partialBuy.length == 0)
                partialBuy = undefined
            return JSON.stringify({
                curPrice,
                openBuyOrders: [...openBuyOrders],
                openSellOrders: [...openSellOrders],
                totalTrades: this.totalTradesSinceStart,
                partialBuy,
                gridMaxLimit: this.gridConfig.hot_grid_max_limit,
                ...this.getCurMoneyState()
            }, null, 2)
        })

        telegramBot.attachCommandListener(Events.TelegramCommand.FORCE_REFRESH, async () => {
            clearTimeout(this.configUpdaterTimeOut)
            await this.periodicGridAndConfigUpdater()
        })
    }


    async periodicGridAndConfigUpdater() {
        await this.refreshGridConfig()
        await this.refreshAndUpdateOldHotGridOrders()
        // Need to timeout id to cancel in Unit test
        this.configUpdaterTimeOut = setTimeout(this.periodicGridAndConfigUpdater.bind(this), this.gridConfig.hot_grid_refresh_interval * 60 * 60 * 1000 || 60 * 60 * 1000)
    }

    /*
        This should be the only function which will place buy order
    */
    async processUpdatedHotGrid() {
        if (!this.gridConfig.is_buy_allowed) {
            logger.info("Buy not allowed. Not placing further buy order", { trade })
            return
        }
        let eachGridAmount = exactMath.round(exactMath.div(this.curWorth, this.curHotGridsAll.size), -2)
        let gridOrderToProcess = await this.getGridOrderToProcess(this.curHotGrids)
        logger.debug(`Processing updated hot grid`, { eachGridAmount, moneyState: this.getCurMoneyState(), gridOrderToProcess })
        for (let hotGrid of gridOrderToProcess) {
            if (this.gridToTradeMap.has(hotGrid)) {
                logger.info(`Order already present for this grid`, { grid: hotGrid })
                continue;
            }
            let amountToUse = Math.min(eachGridAmount, this.remainingMoney)
            if (amountToUse <= 100) {
                logger.info("No money to buy for grid", { grid: hotGrid, curWorth: this.curWorth, remainingMoney: this.remainingMoney })
                break;
            }
            let balance = await this.exchange.getBalance()
            if(balance["INR"].balance < amountToUse){
                logger.error(`Amount mismatch between bot and coindcx balance. not buying`, {balance: balance["INR"], ...this.getCurMoneyState()})
                telegramBot.sendErrorMessage(`Amount mismatch. Ennada pannivechirkinga loosungala!!!`, {balance: balance["INR"], ...this.getCurMoneyState()})
                break;
            }
            let buyOrder = await this.exchange.placeOrder('buy', this.constructBuyRequest(amountToUse, hotGrid));

            this.tradeMap.set(buyOrder.id, new Trade(hotGrid, buyOrder, amountToUse))
            this.gridToTradeMap.set(hotGrid, this.tradeMap.get(buyOrder.id))
            this.remainingMoney = exactMath.sub(this.remainingMoney, amountToUse)
            await this.wait(FILE_CONFIG.waitBetweenBuy)
        }
    }

    constructBuyRequest(amountToUse, hotGrid) {
        let amountToUseWithoutFee = exactMath.div(exactMath.mul(amountToUse, 1000), 1001)
        let quantity = exactMath.div(amountToUseWithoutFee, hotGrid)
        quantity = Math.trunc(quantity * 100) / 100

        logger.info("Buy order placed planned quantity and purchased quantity", { amountToUse, amountToUseWithoutFee, hotGrid });
        return { type: 'buy', limit: hotGrid, quantity }
    }

    async refreshAndUpdateOldHotGridOrders() {
        logger.info(`Refreshing hot grids`)
        let [newHotGrids, newHotGridsAll] = await this.getCurHotGrids()
        let ordersToCancelInCurHotGrid = []
        let gridsCancelled = []
        let reclaimedINRFromCancelingBuyOrder = 0.0;

        for (let curHotGrid of [...this.curHotGrids]) {
            let trade = this.gridToTradeMap.get(curHotGrid)
            //TODO : Handle partially filled and cancelled.
            if (trade && !trade.sellOrder && trade.buyOrder.status == Events.OrderStatus.OPEN) {
                ordersToCancelInCurHotGrid.push(trade.buyOrderId)
                gridsCancelled.push(curHotGrid)
                reclaimedINRFromCancelingBuyOrder += trade.amountBorrowed
                //Clean up
                this.tradeMap.delete(trade.buyOrderId)
                this.gridToTradeMap.delete(curHotGrid)
            }
        }
        if (ordersToCancelInCurHotGrid.length != 0) {
            logger.info(`Cancelling old hot grid buy orders`, { ordersToCancelInCurHotGrid, reclaimedINRFromCancelingBuyOrder, gridsCancelled })
            await this.exchange.cancelOrders({ ids: ordersToCancelInCurHotGrid })
            reclaimedINRFromCancelingBuyOrder = exactMath.round(reclaimedINRFromCancelingBuyOrder, -2)
            this.remainingMoney += reclaimedINRFromCancelingBuyOrder
        }
        logger.info(`Updating hot grids`, { curHotGridsAll: [...this.curHotGridsAll], newHotGridsAll: [...newHotGridsAll], newHotGrids: [...newHotGrids] })
        this.curHotGrids = newHotGrids
        this.curHotGridsAll = newHotGridsAll
        await this.processUpdatedHotGrid()
    }

    async getCurHotGrids() {
        let curTicker = await this.exchange.getCurTicker();
        let curPrice = parseFloat(curTicker.lastPrice)
        let startGrid = exactMath.round(exactMath.sub(curPrice, this.gridConfig.grid_width || 1.0), -2)
        let endGrid = exactMath.round(exactMath.add(curPrice, this.gridConfig.grid_width || 1.0), -2)
        let newHotGrids = new Set()
        let newHotGridsAll = new Set()

        while (startGrid <= endGrid) {
            if (startGrid <= this.gridConfig.hot_grid_max_limit) {
                newHotGrids.add(startGrid)
            }
            newHotGridsAll.add(startGrid)
            startGrid = exactMath.round(exactMath.add(startGrid, this.gridConfig.grid_variation), -2)
        }
        return [newHotGrids, newHotGridsAll, curTicker];
    }

    registerEventListeners() {
        //  this.exchange.on(Events.TICK, this.processTick.bind(this))
        this.exchange.on(Events.BUY_ORDER_FILLED, this.processBuyOrderFillEvent.bind(this))
        this.exchange.on(Events.SELL_ORDER_FILLED, this.processSellOrderFillEvent.bind(this))
        this.exchange.on(Events.BUY_ORDER_PARTIALLY_FILLED, this.processOrderPartiallyFillEvent.bind(this))
    }

    async processOrderPartiallyFillEvent(updatedOrder) {
        let orderId = updatedOrder.id
        if(!this.tradeMap.has(orderId)) {
            logger.warn(`Buy order not found`, orderId)
            return;
        }
        let trade = this.tradeMap.get(orderId)
        logger.info(`${updatedOrder.side} order partially filled`, orderId)
        let order = updatedOrder.side == "buy" ? trade.buyOrder : trade.sellOrder
        Object.assign(order, updatedOrder)
    }

    async processBuyOrderFillEvent(updatedBuyOrder) {
        let buyOrderId = updatedBuyOrder.id
        if (!this.tradeMap.has(buyOrderId)) {
            logger.warn("Buy order not found", buyOrderId)
            return
        }
        let trade = this.tradeMap.get(buyOrderId)
        logger.info("Buy order filled", updatedBuyOrder)

        let sellPrice = exactMath.round(exactMath.add(trade.grid, this.gridConfig.sell_variation_limit), -2)
        let sellOrder = await this.exchange.placeOrder('sell', { limit: sellPrice, quantity: trade.buyOrder.totalQuantity })

        trade.sellOrderId = sellOrder.id
        trade.sellOrder = sellOrder
        trade.buyOrder = Object.assign(trade.buyOrder, updatedBuyOrder)

        this.tradeMap.set(trade.sellOrderId, trade)
    }

    async processSellOrderFillEvent(updatedSellOrder) {
        let sellOrderId = updatedSellOrder.id
        let trade = this.tradeMap.get(sellOrderId)
        trade.sellOrder = Object.assign(trade.sellOrder, updatedSellOrder)
        if (!this.tradeMap.has(sellOrderId)) {
            logger.warn("Sell Order not found", { sellOrderId })
            return
        }

        logger.info(`Sell Order filled`, trade)

        //Es data for tracking...
        logger.debug("writing to ES", sellOrderId);
        let profitDetails = trade.calculateProfit()
        this.curWorth = this.curWorth + profitDetails.profitInINR
        this.remainingMoney += trade.amountBorrowed + profitDetails.profitInINR
        logger.info(`profit details`, { profitDetails, trade })
        this.totalTradesSinceStart++
        try {
            await this.esClient.saveTradeData(FILE_CONFIG.esTradeDataIndex, {
                "timestamp": trade.sellOrder.updatedAt,
                "trade_time": new Date().toISOString(),
                "trade_status": trade.sellOrder.status,
                "trade_percentage": this.gridConfig.sell_variation_limit, // %value 
                "trade_amount": profitDetails.totalBuyAmount,
                "bot_range": trade.buyOrder.pricePerUnit,
                "is_hot_grid": true,

                "profit_in_inr": profitDetails.profitInINR,
                "profit_in_percentage": profitDetails.profitPercent,
                "trading_fees_in_inr": profitDetails.totalTradingFees,
                "total_buy_amount": profitDetails.totalBuyAmount,
                "amount_from_sell": profitDetails.amountFromSell,
                "amount_borrowed": trade.amountBorrowed,
                "amount_used_in_buy": exactMath.sub(trade.amountBorrowed, profitDetails.totalBuyAmount)
            });
            telegramBot.sendInfoMessage(`Trade completed @${trade.buyOrder.pricePerUnit} with a profit of ₹${profitDetails.profitInINR}`)
        } catch (err) {
            logger.error("ES persist failed", err)
        }

        //Clean after successful trade
        this.tradeMap.delete(trade.buyOrderId)
        this.tradeMap.delete(trade.sellOrderId)
        this.gridToTradeMap.delete(trade.grid)

        logger.info(`Trade complete`, { trade, moneyState: this.getCurMoneyState() })
        await this.refreshGridConfig()
        if (!this.gridConfig.is_buy_allowed) {
            logger.info("Buy not allowed. Not placing further buy order", { trade })
            return
        }
        //Process unused grids
        await this.refreshAndUpdateOldHotGridOrders()
    }

    async refreshGridConfig() {
        logger.info(`Refreshing config`)
        let gridConfig = await this.esClient.getConfig("forex-usdt-inr", FILE_CONFIG.esConfigId)
        let new_overall_amount = gridConfig.overall_amount
        let diff = this.gridConfig.overall_amount ? new_overall_amount - this.gridConfig.overall_amount : 0;
        /*
            Three variables which keep track of the money
            1. overall_amount - Amount allocated by overlords. Read only
            2. curWorth - Current total money including the profits
            3. remainingMoney - Amount in INR. Rest of amount is in USDT
        */
        if (diff != 0) {
            logger.info(`Overall amount changing.`, { diff, new_overall_amount, curMoneyState: this.getCurMoneyState() })
            if (diff < 0)
                logger.warn(`Overall amount reduced. Might result in unexpected behaviour`)
        }
        this.curWorth = this.curWorth != undefined ? this.curWorth + diff : new_overall_amount
        this.remainingMoney = this.remainingMoney != undefined ? Math.max(this.remainingMoney + diff, 0) : new_overall_amount
        this.gridConfig = gridConfig
    }

    getCurMoneyState() {
        return {
            overallAmount: this.gridConfig.overall_amount,
            curWorth: this.curWorth,
            remainingMoney: this.remainingMoney
        }
    }

    async getGridOrderToProcess(hotGrids = []) {
        let curTicker = await this.exchange.getCurTicker();
        let curPrice = parseFloat(curTicker.lastPrice)
        /*
            Grid which is closest to curTicker will be processed first. 
            If at equal distance lower grid will come first
        */
        let minHeap = new Heap((grid1, grid2) => {
            let diff1 = Math.abs(curPrice - grid1);
            let diff2 = Math.abs(curPrice - grid2);
            if (Math.abs(diff1) == Math.abs(diff2))
                return grid1 - grid2;
            else
                return diff1 - diff2;
        });

        [...hotGrids].forEach(grid => {
            minHeap.push(grid)
        })
        minHeap.heapify()
        //return minHeap.toArray()
        let orderOfGridsToProcess = []
        while (!minHeap.empty()) {
            orderOfGridsToProcess.push(minHeap.pop())
        }
        return orderOfGridsToProcess;
    }

    async wait(ms) {
        return  new Promise(resolve => setTimeout(resolve, ms));
    }
}

class Trade {
    constructor(grid, buyOrder, amountBorrowed) {
        this.grid = grid;
        this.buyOrder = buyOrder;
        this.buyOrderId = buyOrder.id
        this.amountBorrowed = amountBorrowed
    }

    calculateProfit() {
        let totalBuyAmount = exactMath.add(exactMath.mul(this.buyOrder.totalQuantity, this.buyOrder.avgPrice), this.buyOrder.feeAmount)
        let amountFromSell = exactMath.sub(exactMath.mul(this.sellOrder.totalQuantity, this.sellOrder.avgPrice), this.sellOrder.feeAmount)

        let profitInINR = exactMath.sub(amountFromSell, totalBuyAmount)
        let profitPercent = exactMath.round(exactMath.mul(exactMath.div(profitInINR, totalBuyAmount), 100), -2)
        let totalTradingFees = exactMath.add(this.buyOrder.feeAmount, this.sellOrder.feeAmount)
        return { profitInINR, profitPercent, totalTradingFees, totalBuyAmount, amountFromSell }
    }
}

module.exports = MovingGridBot