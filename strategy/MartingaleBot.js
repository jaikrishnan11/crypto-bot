const logger = require('winston')
const CONFIG = require('config').martingaleBot
const exactMath = require('exact-math')
const telegramBot = require('../clients/telegram-client')

class MartingaleBot {

  constructor(binance) {
    this.binance = binance
    this.init()
  }

  async init() {
    this.config = CONFIG
    await this.attachListeners()
    await this.binance.init()

    await this.initGrids()
  }

  async attachListeners(){
    this.binance.on('BUY_FILLED', this.onBuyFilled.bind(this))
    this.binance.on('SELL_FILLED', this.onSellFilled.bind(this))
  }

  async initGrids() {
    this.state  = {
      totalAmount: this.config.totalAmount,
      totalQtyBought: 0.0,
      startingAmount: this.calculateStartingAmount(this.config.totalAmount, this.config.noOfLevels, this.config.rMultiplier),
      sellOrderTrade : undefined,
      tradeMap: {}
    }

    const currentPrice = await this.binance.futuresPrices()
    logger.info(`Initializing grid @${currentPrice}`, {config: this.config})

    for(let curLevel = 1, levelPrice = currentPrice, levelAmount = this.state.startingAmount; curLevel <= this.config.noOfLevels; curLevel++, levelAmount *= this.config.rMultiplier){
      let buyOrder;
      let levelQuantity = this.getQtyForPrice(levelPrice, levelAmount)
      if(curLevel == 1){
        // Similar to Market order 
        buyOrder = await this.binance.getMarketLongContract(levelQuantity)
      }
      else if(curLevel <= this.config.noOfLevels - this.config.noOfTrailingLevelFromBottom) {
        // Limit order
        buyOrder = await this.binance.getLongContract(levelQuantity, levelPrice)
      }
      else {
        // Trailing Buy
        // Calculate the activation price

        // ap - ActivationPrice, lp - LevelPrice
        // ap + ap * callBackRateBuy/100 =  levelPrice
        // ap ( 1+ cb/100) = lp
        // ap = lp * 100 / (100 + cb)
        let activationPrice = exactMath.round(exactMath.div(levelPrice*100, 100 + this.config.callBackRateBuy), -2)
        buyOrder = await this.binance.placeTrailingBuyOrder(levelQuantity, this.config.callBackRateBuy, activationPrice)
      }

      this.state.tradeMap[buyOrder.clientOrderId] = new Trade(curLevel, levelPrice, buyOrder)

      levelPrice = exactMath.round(exactMath.sub(levelPrice, exactMath.mul(levelPrice, this.config.sellWidthInPercentage/100)), -2) 
    }
    logger.debug(`Initialization done. All the best for this run`, {startingAmount: this.state.startingAmount})
    await telegramBot.sendInfoMessage(`Initialization done. All the best for this run. startingAmount: ${this.state.startingAmount}`)
  }

  async onBuyFilled(updatedBuyOrder) {
    let trade = this.state.tradeMap[updatedBuyOrder.clientOrderId]
    if(!trade){
      logger.warn(`BuyOrder not found`, {updatedBuyOrder})
      return
    }
    trade.buyOrder = Object.assign(trade.buyOrder, updatedBuyOrder)
    logger.info(`BuyOrder Filled for level ${trade.level} `, updatedBuyOrder)
    await telegramBot.sendInfoMessage(`BuyOrder Filled for level ${trade.level}`)

    this.state.totalQtyBought = exactMath.round(exactMath.add(this.state.totalQtyBought, trade.getBuyQty()), -3)

    await this.cancelOpenSellOrders()

    let sellPercent = this.config.sellWidthInPercentage * (trade.isTrailingLevel() ? 2 : 1) / 100 ; // TODO: Check this trailing Sell logic
    let sellPrice = exactMath.round(exactMath.add(trade.getBuyPrice(), exactMath.mul(trade.getBuyPrice(), sellPercent)), -2)
    let sellOrder = await this.binance.releaseLongContract(this.state.totalQtyBought, sellPrice)
    trade.sellOrder = sellOrder
    logger.info(`Place sell order @${sellPrice} totalQty: ${this.state.totalQtyBought}`)
    this.state.tradeMap[sellOrder.clientOrderId] = trade
  }

  async onSellFilled(updatedSellOrder) {
    logger.info(`SellOrder Filled`, updatedSellOrder)
    await telegramBot.sendInfoMessage(`SellOrder Filled`)
    await this.binance.cancelOpenOrders()
    logger.info(`Resetting levels...`)
    await this.initGrids()
  }

  async cancelOpenSellOrders() {
    let sellOrderIds = []
    for(let trade of Object.values(this.state.tradeMap)){
      if(trade.sellOrder){
        sellOrderIds.push(trade.sellOrder.clientOrderId)
        await this.binance.cancelOrderId(trade.sellOrder.clientOrderId)
      }
    }
    // TODO : library doesnt support bulk cancel yet. But API has support for bulk cancel
    logger.info(`Cancelled Sell Orders`, sellOrderIds)
  }

  getQtyForPrice(curPrice, amountToInvest){
    let quantity = exactMath.round(exactMath.div(amountToInvest, curPrice), -3)
    return quantity;
  }

  calculateStartingAmount(totalAmount, noOfLevels, multiplier = 2){
    //totalAmount / (multiplier^noOfLevels - 1)
    let startingAmount =  exactMath.div(totalAmount, exactMath.pow(multiplier, noOfLevels) - 1)
    return exactMath.round(startingAmount, -2)
  }
}

class Trade {
  constructor(level, buyPrice, buyOrder, metaData = {}) {
    this.level = level;
    this.buyPrice = buyPrice;
    this.buyOrder = buyOrder;
    this.metaData = metaData
  }

  getBuyPrice(){
    return this.buyPrice
  }

  getBuyQty() {
    return this.buyOrder.origQty
  }

  isTrailingLevel() {
    return this.level > CONFIG.noOfLevels - CONFIG.noOfTrailingLevelFromBottom 
  }
}

module.exports = MartingaleBot