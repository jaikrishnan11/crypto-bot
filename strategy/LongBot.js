
const telegramBot = require('../clients/telegram-client')
const CONFIG = require('config').get('longShortBot')
const exactMath = require('exact-math')
const {calculateGrids, wait } =require('./utils')
const logger = require('winston')
const Events = require('../exchange/Events')
const _ = require('lodash')

/*
TODO: 
1. [P0] Trailing stop order
2. [P1] Connection reset
3. [P0] Error handling
4. [P1] event handlers should be idompotent.
*/

class LongBot {
  constructor(binance) {
    this.binance = binance
    this.state = {}
    this.init()
  }

  async init(){
    await this.binance.init()
    await this.attachListeners()
    await this.constructState()
    setTimeout(this.processMovingGrid.bind(this),  60 * 60 * 1000)
    //await this.userTradeDetails()
  }

  async attachListeners(){
    this.binance.on('BUY_FILLED', this.onBuyFilled.bind(this))
    this.binance.on('SELL_FILLED', this.onSellFilled.bind(this))
    telegramBot.attachCommandListener(`/getlonggrid`, this.getGrids.bind(this))
    telegramBot.attachCommandListener(`/movelongrange`, this.moveGridsCommand.bind(this))
  }

  async moveGridsCommand(gridRange) {
    logger.info(`Got command to shift grid range ${gridRange}`);
  }

  async getGrids() {
    return this.state.grids
  }

  async constructState() {
    if(CONFIG.restoreState) {

    }
    else {
      this.state = {
        gridDetails: {
          start: CONFIG.gridDetails.start,
          end: CONFIG.gridDetails.end
        },
        noOfTradesSoFar: 0,
        grids: calculateGrids(CONFIG.gridDetails),
        gridToTradeMap: {},
        totalRealisedpnl: 0,
        totalCommission: 0,
        lastReadUserTradeTime: new Date().getTime()
      }
      await this.initializeGrids()
      await this.processUnProcessedTrades()
    }
  }

  async initializeGrids() {
    this.state.eachGridMoney = exactMath.round(exactMath.div(CONFIG.initialAmount, this.state.grids.length), -2);
    this.state.grids.forEach(async (grid) => {
      let quantity = exactMath.round(exactMath.div(this.state.eachGridMoney, grid), -3)
      this.state.gridToTradeMap[grid] = new Trade(null, null, {buyPrice: grid, buyQty: quantity})
    })
  }

  async userTradeDetails() {
    let incomes = await this.binance.futuresUserTrades({startTime: this.state.lastReadUserTradeTime})
    if(incomes.length != 0){
      logger.info(`income data`, {incomes})
      this.state.lastReadUserTradeTime = new Date().getTime()
    }
    setTimeout(this.userTradeDetails.bind(this), 5000)
  }

  async processUnProcessedTrades() {
    let unProcessedTrades = []
    let ticker = await this.binance.futureMarkPrice("ETHUSDT")
    let curPrice = parseFloat(ticker.markPrice);
    for(let grid of Object.keys(this.state.gridToTradeMap)){
      let trade  = this.state.gridToTradeMap[grid]
      if(trade.status == Events.ORDER_NOT_CREATED)
        unProcessedTrades.push(grid)
    }
    if(unProcessedTrades.length == 0){
      logger.info(`No UnProcessed trades left`)
      return
    }

    for(let grid of unProcessedTrades){

      let trade = this.state.gridToTradeMap[grid]
      if(trade.buyPrice > curPrice + CONFIG.stoplimitThreshold)
        continue;
      let buyOrder = await this.binance.getLongContract(trade.buyQty, trade.buyPrice)
      if(buyOrder.code && buyOrder.code < 0) {
        logger.error(`buyorder failed`, buyOrder)
        throw 'Some error placing order'
      }
      logger.info(`BuyOrder for ${buyOrder.price}`)
      delete this.state.gridToTradeMap[grid]
      this.state.gridToTradeMap[buyOrder.price] = new Trade(buyOrder.clientOrderId, buyOrder)
      
    }
    setTimeout(this.processUnProcessedTrades.bind(this), 1000)
  }

  async onBuyFilled(updatedBuyOrder){
    if(!this.state.gridToTradeMap[updatedBuyOrder.originalPrice]){
      logger.warn(`Order not found`, updatedBuyOrder)
      return
    }
    let retryCount = 0;
    while(this.state.gridToTradeMap[updatedBuyOrder.originalPrice].status == Events.ORDER_NOT_CREATED){
      await wait(500)
      retryCount++;
      if(retryCount > 100){
        telegramBot.sendErrorMessage(`[LongBot] onBuyfilled retry failed`);
        logger.error(`[LongBot] onBuyFilled retry failed`, updatedBuyOrder)
        return
      }
    }
    logger.info(`Buy order filled ${updatedBuyOrder.originalPrice}`, updatedBuyOrder)
    let trade = this.state.gridToTradeMap[updatedBuyOrder.originalPrice];
    let buyOrder = Object.assign(this.state.gridToTradeMap[updatedBuyOrder.originalPrice].buyOrder, updatedBuyOrder)
    
    const buyPrice = parseFloat(buyOrder.originalPrice)
    const buyQty = parseFloat(buyOrder.origQty)
    if(buyPrice == NaN){
      logger.error(`BuyPrice parse error`)
      throw `BuyPrice parse error`
    }

    let sellOrder = await this.binance.releaseLongContract(buyQty, buyPrice + CONFIG.gridDetails.sellVariationInUnit)
    trade.sellOrder = sellOrder
    this.state.gridToTradeMap[sellOrder.clientOrderId] = trade
  }

  async onSellFilled(updatedSellOrder){
    if(!this.state.gridToTradeMap[updatedSellOrder.clientOrderId]){
      logger.warn(`Sell Order not found`, updatedSellOrder)
      return
    }
    let trade = this.state.gridToTradeMap[updatedSellOrder.clientOrderId]
    Object.assign(trade.sellOrder, updatedSellOrder)
    let buyOrder = trade.buyOrder
    const buyPrice = parseFloat(buyOrder.originalPrice)
    const buyQty = parseFloat(buyOrder.origQty)
    this.state.noOfTradesSoFar++;

    let profitDetails = trade.getProfitDetails()
    this.state.totalRealisedpnl += profitDetails.realisedpnl;
    this.state.totalCommission += profitDetails.commission;
    logger.info(`TradeCompleted @${trade.buyOrder.originalPrice} soFar: ${this.state.noOfTradesSoFar}`, {trade, profitDetails, totalRealisedpnl: this.state.totalRealisedpnl, commission: this.state.totalCommission})
    await telegramBot.sendInfoMessage(`Trade Completed @${buyPrice}. soFar: ${this.state.noOfTradesSoFar} totalPNL: ${this.state.totalRealisedpnl} totalComission: ${this.state.totalCommission} ${JSON.stringify(profitDetails)} `)
    
    delete this.state.gridToTradeMap[updatedSellOrder.clientOrderId]
    buyOrder  = await this.binance.getLongContract(buyQty, buyPrice)
    this.state.gridToTradeMap[buyOrder.price] = new Trade(buyOrder.clientOrderId, buyOrder)
    logger.info(`Buy Order placed @${buyPrice}`)
  }

  async moveGrid(doesGridNeedToBeMoved, generateNewGrids) {
    let gridsToBeMoved = _.filter(this.state.grids, doesGridNeedToBeMoved);
    
    let ethToBeSoldAtLoss = 0.0;
    logger.info(`Going to move grids`, {gridsToBeMoved})
    for(let grid of gridsToBeMoved){
      let trade = this.state.gridToTradeMap[grid];
      let orderToCancel = trade?.sellOrder?.orderId || trade?.buyOrder?.orderId
      ethToBeSoldAtLoss += (trade?.sellOrder ? trade.buyQty : 0.0)
      if(orderToCancel) {
        // What if order filled before cancelling. EDGE CASE
        let result = await this.binance.cancelOrderId(orderToCancel)
        logger.info(`Order @${grid} cancelled`, {result})
      }
      delete this.state.gridToTradeMap[grid]
      delete this.state.gridToTradeMap[trade?.sellOrder?.clientOrderId]
    }

    if(ethToBeSoldAtLoss > 0.0) {
      let sellOrder = await this.binance.releaseLongContract(ethToBeSoldAtLoss);
      logger.info(`Market Sell order completed`, {sellOrder, ethToBeSoldAtLoss})
      telegramBot.sendInfoMessage(`Sold ${ethToBeSoldAtLoss} ETH at loss :(`)
    }

    let newGrids = _.filter(this.state.grids, grid => !doesGridNeedToBeMoved(grid))
    generateNewGrids(gridsToBeMoved.length).forEach(newGrid => {
      newGrid = exactMath.round(newGrid, -2);
      let quantity = exactMath.round(exactMath.div(this.state.eachGridMoney, newGrid), -3)
      this.state.gridToTradeMap[newGrid] = new Trade(null, null, {buyPrice: newGrid, buyQty: quantity})
      newGrids.push(newGrid)
    })

    Object.assign(this.state.gridDetails, {start: _.min(newGrids), end: _.max(newGrids)})
    this.state.grids = _.sortBy(newGrids)
  }

  async processMovingGrid() {
    let ticker = await this.binance.futureMarkPrice("ETHUSDT")
    let curPrice = 9000 || parseFloat(ticker.markPrice);
    let leftMostGrid = this.state.gridDetails.start;
    let rightmostGrid = this.state.gridDetails.end;
    let totalWidth = rightmostGrid - leftMostGrid;
    let gridWidth = Math.abs(this.state.grids[0] - this.state.grids[1])

    if(curPrice + 20 < leftMostGrid) {
      logger.info(`[LongBot] Moving grid from right to left. a.k.a moving with loss `)
      telegramBot.sendInfoMessage(`[LongBot] Moving grid from right to left. a.k.a moving with loss`)
      const threshold = rightmostGrid - totalWidth * (CONFIG.movingGridPercentage / 100);
      const doesGridNeedToBeMoved = grid =>  grid >= threshold
      const generateNewGrids = noOfNewGrids => _.range(leftMostGrid - gridWidth, leftMostGrid - gridWidth * noOfNewGrids - 1, gridWidth * - 1)
      await this.moveGrid(doesGridNeedToBeMoved, generateNewGrids)
    }
    else if(curPrice - 20 > rightmostGrid) {
      logger.info(`[LongBot] Moving grid from left to right. `)
      telegramBot.sendInfoMessage((`[LongBot] Moving grid from left to right.`))
      const threshold = leftMostGrid + totalWidth * (CONFIG.movingGridPercentage / 100);
      const doesGridNeedToBeMoved = grid =>  grid <= threshold
      const generateNewGrids = noOfNewGrids => _.range(rightmostGrid + gridWidth, rightmostGrid +  gridWidth * noOfNewGrids + 1, gridWidth )
      await this.moveGrid(doesGridNeedToBeMoved, generateNewGrids)
    }
    else {
      logger.debug(`[LongBot] Bot within range. All is Well :)`)
    }
    setTimeout(this.moveGrid.bind(this), CONFIG.movingGridCheckTime * 60 * 1000)
  }
}

class Trade {
  constructor(id, buyOrder, data = {}){
    this.id = id;
    this.buyOrder = buyOrder
    if(buyOrder){
      this.buyPrice = parseFloat(buyOrder.price)
      this.buyQty = parseFloat(buyOrder.origQty)
    }
    this.status = buyOrder ? Events.ORDER_CREATED : Events.ORDER_NOT_CREATED 
    Object.assign(this, data)
  }

  getProfitDetails() {
    return {
      realisedpnl: parseFloat(this.sellOrder.realizedProfit),
      commission: parseFloat(this.sellOrder.commission)
    }
  }
}


module.exports = LongBot