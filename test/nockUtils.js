const { expect } = require('chai');
const nock = require('nock')

let buyCounter = {}, sellCounter = {};

const Fixtures = {
    createOrderResponse: require('./fixtures/createOrderResponse'),
    orderStatusResponse: require('./fixtures/orderStatusResponse')[0],
    esConfig: require('./fixtures/esConfig.json'),
    getTicker: require('./fixtures/tickerResponse.json'),
    getBalance: JSON.parse(JSON.stringify(require('./fixtures/getBalancesResponse.json')))
}
let curTicker = "77.91"

module.exports = {
    stubCreateOrder: function () {
        nock(/localhost/).persist(true)
            .post("/exchange/v1/orders/create")
            .reply(200, (uri, requestBody) => {
                let counter = requestBody.side == 'buy' ? buyCounter : sellCounter;
                counter[requestBody.price_per_unit] = (counter[requestBody.price_per_unit] || 0) + 1
                let id = counter[requestBody.price_per_unit]
                id =  requestBody.side == 'buy' ? `buy-${requestBody.price_per_unit}-${id}` : `sell-${requestBody.price_per_unit}-${id}`
                this.createdOrdersSoFar.push(id)
                return {
                    orders: [Object.assign({}, Fixtures.createOrderResponse.orders[0], {
                        "id": id,
                        "total_quantity": requestBody.total_quantity,
                        "remaining_quantity": requestBody.remaining_quantity,
                        "price_per_unit": requestBody.price_per_unit,
                        "side": requestBody.side
                    })]
                }
            }, { "content-type": "application/json" })
    },

    simulateOrderFill: function (trades = [], side = 'buy') {
        let response = trades.map(trade => {
            let order = trade.buyOrder
            if(side == 'sell'){
             order = trade.sellOrder
             expect(order).to.not.be.null
            }
            return Object.assign({}, Fixtures.orderStatusResponse, {
                "id": order.id,
                "total_quantity": order.totalQuantity,
                "remaining_quantity": 0,
                "price_per_unit": order.pricePerUnit,
                "avg_price": order.pricePerUnit,
                "status": "filled",
                "fee_amount": (order.pricePerUnit * order.totalQuantity * 0.1)/100
            })
        })

        nock(/localhost/).post("/exchange/v1/orders/status_multiple")
                         .reply(200, response)
    },

    setTicker: function(price) {
        curTicker = price
        nock(/localhost/).persist(true)
                         .get("/exchange/ticker")
                         .reply(200, ()=> {
                            let ticker = JSON.parse(JSON.stringify(Fixtures.getTicker))
                            ticker[0].last_price = curTicker 
                            return ticker
                         })
    },
    changeTicker: function(price){
        curTicker = price
    },
    cancelOrders: function() {
        nock(/localhost/).persist(true)
                         .post("/exchange/v1/orders/cancel_by_ids")
                         .reply(200, (uri, body) => {
                            this.lastCancelledIds = body.ids
                         })
    },
    getBalance: function() {
        nock(/localhost/).persist(true)
                        .post("/exchange/v1/users/balances")
                        .reply(200, (uri, body) => {
                           Fixtures.getBalance[0] = this.balanceOverride.INR
                           return Fixtures.getBalance
                        })
    },
    balanceOverride: {
        "INR": {
            "balance": "160200.1046182",
            "locked_balance": "0.0",
            "currency": "INR"
        }
    },
    lastCancelledIds: [],
    createdOrdersSoFar: []
}