const fs = require('fs')
const CoinDCX = require('../exchange/CoinDCX')
const CONFIG = require('config').get('coindcxExchange')
const nock = require('nock')
const { expect } = require('chai')
const chai = require('chai')
const sinonChai = require("sinon-chai");
chai.use(sinonChai)
const Events = require('../exchange/Events')
const sinon = require('sinon')
const exp = require('constants')
const logger = require('winston')
const telegram = require('../clients/telegram-client')
const wtf = require('wtfnode')

const Fixtures = {
  createOrderResponse: require('./fixtures/createOrderResponse'),
  orderStatusResponse: require('./fixtures/orderStatusResponse'),
  getTicker: require('./fixtures/tickerResponse.json'),
  getBalance: require('./fixtures/getBalancesResponse.json')
}

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

describe('CoinDCX exchange', function () {
  let exchange;

  this.beforeEach(function () {
    nock.cleanAll()
    exchange = new CoinDCX()
  })

  afterEach(function () {
    exchange.runPoller = false
    nock.cleanAll()
  })

  describe('Place order', function () {
    it("Place buy order", async function () {
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)

      let response = await exchange.placeOrder('buy', { limit: 77, quantity: 5 })

      expect(exchange.unFulfilledBuyOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
      expect(exchange.unFulfilledSellOrderIds).to.be.empty
      expect(response.id).to.equal("ead19992-43fd-11e8-b027-bb815bcb14ed")
    })

    it("Place sell order", async function () {
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)

      let response = await exchange.placeOrder('sell', { limit: 77, quantity: 5 })

      expect(exchange.unFulfilledSellOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
      expect(exchange.unFulfilledBuyOrderIds).to.be.empty
      expect(response.id).to.equal("ead19992-43fd-11e8-b027-bb815bcb14ed")
    })

    it("Should retry for 429 response code", async function () {
      this.timeout(5000)
      nock(/localhost/).post('/exchange/v1/orders/create')
        .delay(150)
        .reply(429, "Connection Limit Exceeded")
        .post('/exchange/v1/orders/create')
        .delay(150)
        .reply(429, "Connection Limit Exceeded")
        .post('/exchange/v1/orders/create')
        .delay(50)
        .reply(200, Fixtures.createOrderResponse)

      let response = await exchange.placeOrder('sell', { limit: 77, quantity: 5 })
      expect(exchange.unFulfilledSellOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
      expect(exchange.unFulfilledBuyOrderIds).to.be.empty
      expect(response.id).to.equal("ead19992-43fd-11e8-b027-bb815bcb14ed")
    })
  })

  describe('Poll order status', function () {
    let pollOrderStatusEndpoint = "/exchange/v1/orders/status_multiple"

    it('Should not run if runPoller is false', async function () {
      exchange.unFulfilledBuyOrderIds.add("buyOrder")
      let scope = nock(/localhost/).persist(true)
        .post(pollOrderStatusEndpoint)
        .reply(200, [])
      await exchange.startOrderStatusPoller()
      await wait(80)
      expect(scope.isDone()).to.be.false
    })

    it('Should  run if runPoller is true', async function () {
      let scope = nock(/localhost/).persist(true)
        .post(pollOrderStatusEndpoint)
        .reply(200, [])
      exchange.unFulfilledBuyOrderIds.add("1")
      exchange.runPoller = true;
      await exchange.startOrderStatusPoller()
      await wait(41)
      expect(scope.isDone()).to.be.true
    })

    it('Should retry when failed', async function () {
      this.timeout(8000)
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)
      await exchange.placeOrder('buy', { limit: 77, quantity: 5 })


      expect(exchange.unFulfilledBuyOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")

      nock(/localhost/).post(pollOrderStatusEndpoint)
        .reply(400);
      nock(/localhost/).post(pollOrderStatusEndpoint)
        .reply(200);
      nock(/localhost/).post(pollOrderStatusEndpoint)
        .reply(200, Fixtures.orderStatusResponse);
      exchange.runPoller = true;

      let buyOrderFilledEvent = sinon.spy()
      exchange.on(Events.BUY_ORDER_FILLED, buyOrderFilledEvent)
      await exchange.startOrderStatusPoller()
      await wait(200)
      expect(buyOrderFilledEvent.calledOnce).to.be.true
      expect(buyOrderFilledEvent.args[0][0]).to.deep.include({
        status: "filled",
        avgPrice: 77.45,
        feeAmount: 0.298957
      })
      expect(exchange.unFulfilledBuyOrderIds).to.not.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
    })

    it('Should not emit if status is not filled', async function () {
      this.timeout(10000)
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)
        .persist(true)
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "open" }]);
      await exchange.placeOrder('buy', { limit: 77, quantity: 5 })


      let buyOrderFilledEvent = sinon.spy()
      exchange.on(Events.BUY_ORDER_FILLED, buyOrderFilledEvent)
      exchange.runPoller = true;
      await exchange.startOrderStatusPoller()
      await wait(150)
      expect(buyOrderFilledEvent.calledOnce).to.be.false
      expect(exchange.unFulfilledBuyOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
    })

    it('Should poll continuously', async function () {
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "open" }])
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "open" }])
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "filled" }])

      let buyOrderFilledEvent = sinon.spy()
      exchange.on(Events.BUY_ORDER_FILLED, buyOrderFilledEvent)
      exchange.runPoller = true;
      await exchange.startOrderStatusPoller()
      await exchange.placeOrder('buy', { limit: 77, quantity: 5 })
      await wait(80)
      expect(buyOrderFilledEvent.calledOnce).to.be.true
    })

    it('Should emit Sell Order filled', async function () {
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "filled" }]);
      await exchange.placeOrder('sell', { limit: 77, quantity: 5 })
      expect(exchange.unFulfilledSellOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")

      let sellOrderFilledEvent = sinon.spy()
      exchange.on(Events.SELL_ORDER_FILLED, sellOrderFilledEvent)
      exchange.runPoller = true;
      await exchange.startOrderStatusPoller()
      expect(sellOrderFilledEvent.calledOnce).to.be.true
      expect(exchange.unFulfilledSellOrderIds).to.be.empty
    })

    it('Should emit Buy order partially filled', async function () {
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "partially_filled", "side": "buy" }]);
      await exchange.placeOrder('buy', { limit: 77, quantity: 5 })

      let buyOrderFilledEvent = sinon.spy()
      exchange.on(Events.BUY_ORDER_PARTIALLY_FILLED, buyOrderFilledEvent)
      exchange.runPoller = true;
      await exchange.startOrderStatusPoller()
      await wait(11)
      expect(buyOrderFilledEvent).to.have.been.calledOnce
      expect(exchange.unFulfilledBuyOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
      expect(buyOrderFilledEvent.args[0][0]).to.include({ "status": "partially_filled", "id": "ead19992-43fd-11e8-b027-bb815bcb14ed" })
    })

    it('Should emit Sell order partially filled', async function () {
      nock(/localhost/).post('/exchange/v1/orders/create')
        .reply(200, Fixtures.createOrderResponse)
        .post(pollOrderStatusEndpoint)
        .reply(200, [{ "id": "ead19992-43fd-11e8-b027-bb815bcb14ed", "status": "partially_filled", "side": "sell" }]);
      await exchange.placeOrder('buy', { limit: 77, quantity: 5 })

      let sellOrderFilledEvent = sinon.spy()
      exchange.on(Events.SELL_ORDER_PARTIALLY_FILLED, sellOrderFilledEvent)
      exchange.runPoller = true;
      await exchange.startOrderStatusPoller()
      await wait(11)
      expect(sellOrderFilledEvent).to.have.been.calledOnce
      expect(exchange.unFulfilledBuyOrderIds).to.include("ead19992-43fd-11e8-b027-bb815bcb14ed")
    })
  })

  describe('Get Current ticker', function () {
    it('Return cur ticker', async function () {
      nock(/localhost/).get('/exchange/ticker')
        .reply(200, Fixtures.getTicker)
      let ticker = await exchange.getCurTicker()
      expect(ticker).to.include({
        "market": "USDTINR",
        "high": "78.86",
        "low": "76.78",
        "volume": "283207222.56519992114",
        "lastPrice": "77.91",
        "bid": "77.880000",
        "ask": "77.910000",
        "timestamp": 1634302643
      })
    })

    it('Retry when failed', async function () {
      nock(/localhost/).get('/exchange/ticker')
        .reply(400, "Error")
        .get('/exchange/ticker')
        .reply(200, Fixtures.getTicker)
      let ticker = await exchange.getCurTicker()
      expect(ticker).to.include({
        "market": "USDTINR",
        "lastPrice": "77.91"
      })

    })
  })

  describe('Cancel orders', function () {
    it("Should cancel buy order", async function () {
      nock(/localhost/).post("/exchange/v1/orders/cancel_by_ids")
        .reply(200, {})
      exchange.unFulfilledBuyOrderIds = new Set(["order1", "order2", "order3"])
      await exchange.cancelOrders({ ids: ["order1", "order3"] });
      expect([...exchange.unFulfilledBuyOrderIds]).to.have.all.members(["order2"])
    })

    it.skip("Should not cancel sell order", async function () {
      nock(/localhost/).post("/exchange/v1/orders/cancel_by_ids")
        .reply(200, {})
      exchange.unFulfilledBuyOrderIds = new Set(["order0"])
      exchange.unFulfilledSellOrderIds = new Set(["order1", "order2", "order3"])
      await exchange.cancelOrders({ ids: ["order0", "order3"] });
      expect(exchange.unFulfilledBuyOrderIds).to.be.empty
      expect([...exchange.unFulfilledSellOrderIds]).to.have.all.members(["order1", "order2", "order3"])

    })

    it("Should not delete anything if error", async function () {
      let spy = sinon.spy(telegram, 'sendErrorMessage')
      nock(/localhost/).persist(true)
        .post("/exchange/v1/orders/cancel_by_ids")
        .reply(401, {})
      exchange.unFulfilledBuyOrderIds = new Set(["order0"])
      await exchange.cancelOrders({ ids: ["order0", "order3"] });
      expect(spy.calledOnce).to.be.true
      expect(exchange.unFulfilledBuyOrderIds).to.include("order0")
    })
  })

  describe('Get balance', function () {
    it('should return balance map', async function () {
      nock(/localhost/).post('/exchange/v1/users/balances')
        .reply(200, Fixtures.getBalance)
      let balanceMap = await exchange.getBalance()
      expect(balanceMap["INR"]).to.include(
        {
          "currency": "INR",
          "balance": 1602.1046182,
          "lockedBalance": 0
        })
    })

    it('should return balance map after retry', async function () {
      this.timeout(2500)
      nock(/localhost/)
        .post('/exchange/v1/users/balances')
        .reply(400, "some error")
        .post('/exchange/v1/users/balances')
        .reply(200, Fixtures.getBalance)
      let balanceMap = await exchange.getBalance()
      expect(balanceMap["INR"]).to.include(
        {
          "currency": "INR",
          "balance": 1602.1046182,
          "lockedBalance": 0
        })
    })
  })

  describe('Resiliency', function () {
    it('should store and retrieve correctly', function () {
      exchange.unFulfilledBuyOrderIds.add("buy1")
      exchange.unFulfilledSellOrderIds.add("sell1")
      exchange.unFulfilledSellOrderIds.add("sell2")

      exchange.saveState()

      let newExchange = new CoinDCX()
      expect([...newExchange.unFulfilledBuyOrderIds]).to.have.all.members(["buy1"]);
      expect([...newExchange.unFulfilledSellOrderIds]).to.have.all.members(["sell1", "sell2"])
      newExchange.runPoller = false
    })
  })
})

before(function () {
  logger.configure({
    silent: true
  })
  fs.existsSync(CONFIG.stateFilePath) && fs.unlinkSync(CONFIG.stateFilePath)
})

after(function () {
  fs.existsSync(CONFIG.stateFilePath) && fs.unlinkSync(CONFIG.stateFilePath)
})