const fs = require('fs')
const wtf = require('wtfnode')
const chai = require("chai");
const sinonChai = require("sinon-chai");
chai.use(sinonChai)

const CoinDCX = require("../exchange/CoinDCX")
const MovingGridBot = require('../strategy/MovingGridBot')
const ESClient = require('../clients/elastic-client')
const CONFIG = require('config')

const sinon = require('sinon')
const { expect } = require('chai')
const nock = require("nock")
const nockUtils = require('./nockUtils');
const { clearTimeout } = require('timers');
const exp = require('constants');

const Fixtures = {
    esConfig: require('./fixtures/esConfig.json'),
    getTicker: require('./fixtures/tickerResponse.json'),
    getBalance: require('./fixtures/getBalancesResponse.json')
}

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
let stateFilePath = CONFIG.get('movingGridBot').stateFilePath
describe("MovingGridBot", function () {
    let exchange;
    let bot;
    let esClient = sinon.createStubInstance(ESClient);

    before(function () {
        esClient.getConfig.reset()
        esClient.verifyElasticClient.resolves()
        esClient.getConfig.resolves(Object.assign({}, Fixtures.esConfig))

        fs.existsSync(stateFilePath) && fs.unlinkSync(stateFilePath)
    })

    afterEach(function () {
        clearTimeout(bot.configUpdaterTimeOut)
    })

    describe("Initialisation (Without restore)", function () {
        let lastCurWorth;

        before(function () {
            exchange = new CoinDCX()
            bot = new MovingGridBot(exchange, esClient, { initNow: false })

            nockUtils.stubCreateOrder()
            nockUtils.setTicker("77.91")
            nockUtils.getBalance()
            nockUtils.cancelOrders()
        })

        after(function () {
            clearTimeout(bot.configUpdaterTimeOut)
            nock.cleanAll()
            fs.existsSync(stateFilePath) && fs.unlinkSync(stateFilePath)
        })


        it("App initialize and place initial orders", async function () {
            this.timeout(10000)
            await bot.initialize()
            expect(bot.gridConfig).to.deep.eq(Fixtures.esConfig)
            expect(bot.getCurMoneyState()).to.deep.eq({
                "curWorth": 1500,
                "overallAmount": 1500,
                "remainingMoney": 600
            })
            expect(nockUtils.createdOrdersSoFar).to.have.all.members(["buy-76.91-1", "buy-77.41-1", "buy-77.91-1"])
            expect(bot.gridToTradeMap).to.have.all.keys([76.91, 77.41, 77.91])
            expect([...bot.gridToTradeMap.values()].every(t => t.amountBorrowed == 300)).to.be.true

            expect(bot.tradeMap).to.have.all.keys(["buy-76.91-1", "buy-77.41-1", "buy-77.91-1"])
        })

        it("Buy order filled", async function () {
            exchange.runPoller = true;
            nockUtils.simulateOrderFill([bot.gridToTradeMap.get(76.91), bot.gridToTradeMap.get(77.41)])
            await exchange.pollOrderStatus()
            await wait(30)
            expect([...bot.tradeMap.keys()]).to.include.members(["sell-77.41-1", "sell-77.91-1"])
            expect(bot.gridToTradeMap.get(76.91).sellOrder).to.include({
                "pricePerUnit": 77.41
            })
            expect(bot.gridToTradeMap.get(77.41).sellOrder).to.include({
                "pricePerUnit": 77.91
            })

            expect(bot.getCurMoneyState()).to.deep.eq({
                "curWorth": 1500,
                "overallAmount": 1500,
                "remainingMoney": 600
            })
        })

        it("Sell order filled", async function () {
            this.timeout(10000)
            nockUtils.simulateOrderFill([bot.gridToTradeMap.get(76.91)], "sell")
            await exchange.pollOrderStatus()
            await wait(350)

            expect(esClient.saveTradeData).to.have.been.calledOnce
            expect(nockUtils.lastCancelledIds).to.have.all.members(["buy-77.91-1"])
            expect(bot.gridToTradeMap).to.not.have.key(76.91)
            expect(bot.remainingMoney).to.be.greaterThan(600)
            expect(bot.curWorth).to.be.greaterThan(600)
            expect([...bot.tradeMap.keys()]).to.not.include.members(["buy-76.91-1", "sell-77.41-1"])
            expect([...bot.tradeMap.keys()]).to.have.all.members(["buy-77.91-2", "buy-76.91-2", "sell-77.91-1", "buy-77.41-1"])
        })

        it("Update Grids", async function () {
            nockUtils.changeTicker("77")
            nockUtils.createdOrdersSoFar = []
            clearTimeout(exchange.configUpdaterTimeOut)
            await bot.periodicGridAndConfigUpdater()
            expect(nockUtils.lastCancelledIds).to.have.all.members(["buy-77.91-2", "buy-76.91-2"])
            expect(nockUtils.createdOrdersSoFar).to.have.all.members(["buy-77-1", "buy-76.5-1", "buy-77.5-1", "buy-76-1"])
            expect(bot.gridToTradeMap.get(77).amountBorrowed).is.greaterThan(300).lessThanOrEqual(302)
            lastCurWorth = bot.curWorth
        })

        it("Save and Restore correctly", async function () {
            bot.saveState()
            let newExchange = new CoinDCX()
            let newBot = new MovingGridBot(newExchange, esClient, { initNow: false })
            await newBot.restoreState()
            expect([...newBot.tradeMap.keys()]).to.have.all.members(["buy-77-1", "buy-76.5-1", "buy-77.5-1", "buy-76-1", "sell-77.91-1", "buy-77.41-1"])
            expect(newBot.remainingMoney).to.be.greaterThanOrEqual(0)
            expect(newBot.gridConfig.overall_amount).to.be.eq(1500)
        })
    })
})