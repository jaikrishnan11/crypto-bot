const winston = require('winston')
require('winston-daily-rotate-file');
const telegramBot = require('./clients/telegram-client')
const config = require('config');
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const { uniqueNamesGenerator, adjectives, colors, animals, names } = require('unique-names-generator');
const ESClient = require('./clients/elastic-client')

// Strategies
const TraditionalGridBot = require('./strategy/TraditionalGridBot')
const MovingGridBot = require('./strategy/MovingGridBot')
const LongBot = require('./strategy/LongBot');
const ShortBot = require('./strategy/ShortBot');
const MartingaleBot = require('./strategy/MartingaleBot');

// Exchanges
const BinanceExchange = require('./exchange/BinanceExchange')
const CoinDCX = require('./exchange/CoinDCX')
const TestExchange = require('./exchange/TestExchange')

const argv = yargs(hideBin(process.argv)).argv
const botName = uniqueNamesGenerator({ dictionaries: [adjectives, colors], length: 2 })

const timezoned = () => {
  return new Date().toLocaleString('en-US', {
    timeZone: 'Asia/Calcutta'
  });
}

winston.configure({
  transports: [new winston.transports.Console({
    format: winston.format.combine(winston.format.timestamp({ format: timezoned }), winston.format.simple()),
    level: "debug"
  }),
  new winston.transports.DailyRotateFile({
    level: "debug",
    dirname: 'logs',
    filename: `%DATE%--${botName}-bot.log`,
    datePattern: 'MM-DD',
    maxSize: '512m',
    format: winston.format.combine(winston.format.timestamp({ format: timezoned }), winston.format.json())
  })]
})
console.log(config);
winston.info("Starting process with args " + JSON.stringify(argv));

//let exchange = new CoinDCX()
//let exchange = new TestExchange()
//let exchange = new BinanceExchange({symbol: "ETHUSDT", profile: "jaiTestFuture"})

let longExchange = new BinanceExchange({symbol:"ETHUSDT", profile:"sankarTestFuture"})
let shortExchange = new BinanceExchange({symbol:"ETHUSDT", profile:"jaiTestFuture"})
let longBot = new LongBot(longExchange);
let shortBot = new ShortBot(shortExchange);

//let bot = new MartingaleBot(exchange)
//let bot = new MovingGridBot(exchange, new ESClient())
//let bot = new TraditionalGridBot(exchange, new ESClient());


function saveState() {
  //exchange.saveState()
  //bot.saveState()
}

[`uncaughtException`, `unhandledRejection`].forEach(eventType => {
  process.on(eventType, (code, promise) => {
    winston.error("unhandled error", code)
    saveState()
    telegramBot.sendErrorMessage(`Crypto Bot crashing with error code : ${JSON.stringify(code, null, 2)}`).finally(() => process.exit(100))
  });

});

[`SIGINT`, `SIGUSR1`, `SIGUSR2`, `SIGTERM`].forEach((eventType) => {
  process.on(eventType, code => {
    winston.error(code)
    saveState()
    //TODO : getting message two times. Why ?
    telegramBot.sendErrorMessage(`Supposed to run 24/7. Did you kill the process ?  ${code}`).finally(process.exit)
  });
});

process.on(`beforeExit`, code => telegramBot.sendErrorMessage(`Crypto bot shutting down. No more work to do : ${code}`).then(process.exit));
