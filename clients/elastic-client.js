const elasticsearch = require('elasticsearch');
const logger = require("winston")
const util = require('util')
const CONFIG = require('config').get('elasticSearch')
/*
  PUT Config
  hot_gird_variation 
*/
class ElasticsearchClient {
  constructor(opts = {
    // host: 'localhost:9200',
    host: CONFIG.host,
    log: 'error',
    apiVersion: CONFIG.apiVersion, // use the same version of your Elasticsearch instance
  }) {
    this.elasticSearchClient = new elasticsearch.Client(opts);
  }

  verifyElasticClient() {
    this.elasticSearchClient.ping({
      // ping usually has a 3000ms timeout
      requestTimeout: 3000
    }, function (error) {
      if (error) {
        console.trace('elasticsearch cluster is down!');
      } else {
        console.log('All is well');
      }
    });
  }

  async getConfig(forexName = "forex-usdt-inr", configId = "config_default") {
    try {
      let result = await this.elasticSearchClient.get({ "index": forexName, "type": "config", "id": configId });
      return result._source;
    } catch (err) {
      logger.error("getConfig Error in getting a config", err, forexName, configId)
      throw err
    }
  }

  async getBotInfo(forexName = "forex-usdt-inr-bots", botName) {
    try {
      return await this.elasticSearchClient.get({ "index": forexName, "type": "bot-info", "id": botName });
    } catch (err) {
      logger.error("getBotInfo Error in getting a bot info", err, forexName, botName)
      return err
    }
  }

  //yet to be done
  async saveTradeData(indexName = "forex-usdt-inr-trades-test", data) {
    try {
      return await this.elasticSearchClient.index({ "index": indexName, "type": "trades", body: data });
    } catch (err) {
      logger.error("saveTradeStatus Error in saving a trade status", err, indexName, data)
      return err
    }
  }

  async saveTradeLogs(forexName = "forex-usdt-inr-trade-logs", data) {
    if (CONFIG.blockUpdates) {
      logger.info("Updates to ES is blocked. Check blockUpdates config")
      return
    }

    try {
      return await this.elasticSearchClient.index({ "index": forexName, "type": "trade_logs", body: data });
    } catch (err) {
      logger.error("saveTradeLogs Error in saving a trage logs", err, forexName, data)
      return err;
    }
  }


}

// let es = new ElasticsearchClient();
// es.saveTradeData("eth-trading", {
//   "bot_name" : "bot_1",
//   "trade_type" : "LONG", //SHORT
//   "timestamp" : "2021-09-20T19:28:52.141Z",
//   "trade_status": "success",
//   "trade_percentage" : 0.3,
//   "quantity" : 0.02,
//   "amount_invested": 100, //in dollars
//   "bot_start_range" : 50,
//   "bot_end_range" : 60,
//   "profit_in_inr" : 50.23,
//   "profit_in_usdt" : 7.14
// }).then((data) => {
//   console.log(data)
// }).catch((err) => {
//   console.log("error ", err)
// })
module.exports = ElasticsearchClient