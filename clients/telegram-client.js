const TelegramClient = require('node-telegram-bot-api');
const CONFIG = require('config').util.toObject().telegramBot
const Events = require('../exchange/Events')
const logger = require('winston')

class TelegramBot {
    constructor(){
        this.client = new TelegramClient(CONFIG.accessToken, Object.assign({}, CONFIG.constructorConfig));
        this.registeredCommandListeners = {}
        this.noOfMessages = 0
        this.init()
    }

    async init() {

        this.client.onText(/\/status/,  async (msg, [source, match]) => {
            try {
                let listener = this.registeredCommandListeners[Events.TelegramCommand.STATUS]
                if(listener == undefined){
                    logger.warn(`no listener for ${source} command`)
                    return
                }
                let result = await listener()
                this.client.sendMessage(msg.chat.id, result)
            } catch(e) {
                logger.error("Failed to process command ")
            }
        })
        this.client.onText(/\/refresh/,  async (msg, [source, match]) => {
            try {
                let listener = this.registeredCommandListeners[Events.TelegramCommand.FORCE_REFRESH]
                if(listener == undefined){
                    logger.warn(`no listener for ${source} command`)
                    return
                }
                let result = await listener()
                this.sendInfoMessage("Refreshed")
            } catch(e) {
                logger.error("Failed to process command ")
                this.sendErrorMessage(`Failed to refresh. ${e}`)
            }
        })

        this.client.onText(/.*/, async (msg, [source, match]) => {
            try {
                let commandLength = msg?.entities[0]?.length
                let command = msg.text.substring(0, commandLength)
                let listener = this.registeredCommandListeners[command]
                if(listener == undefined){
                    logger.warn(`no listener for ${source} command`)
                    return 
                }
                let result = await listener(source.substring(commandLength+1))
                if(result)
                    this.sendInfoMessage(`${source} - ${JSON.stringify(result)}`)
            } catch (e) {
                logger.error(`Failed to process command ${source}`, e)
                this.sendErrorMessage(`Failed to process command ${source} - ${e}`)
            }
        })
    }

    attachCommandListener(command, listener){
        if(this.registeredCommandListeners[command])
            logger.warn(`${command} already registered. overwriting`)
        this.registeredCommandListeners[command] = listener
    }

    async sendInfoMessage(message) {
        const time30sAgo = new Date(new Date() - new Date(30*1000))
        if(CONFIG.isDisabled || this.lastErrorTime&&(this.lastErrorTime >= time30sAgo)){
            return
        }
        try {
            await this.client.sendMessage(CONFIG.updatesGroupId, message)
            this.noOfMessages++;
            this.lastErrorTime = null
            if(this.noOfMessages >=15){
                this.noOfMessages = 0;
                this.lastErrorTime = new Date()
                logger.warn(`[Telegram] Blocking messages for 30s`, {message})
            }
        } catch (err) {
            logger.error(`[TelegramBot] sendInfoMessage failed. Blocking messages for 30s`);
            this.lastErrorTime = new Date()
        }
    }

    async sendErrorMessage(message, obj ) {
        if(CONFIG.isDisabled){
            return
        }
        // Send message and dont honour isDisabled flag. This is critical
        await this.client.sendMessage(CONFIG.updatesGroupId, `<b>!!! ERROR !!!</b> \n 
            ${message} \n <pre><code class="language-javascript"> ${obj ? JSON.stringify(obj) : ""}</code> </pre>`, {parse_mode : "HTML"})
    }
}

module.exports = new TelegramBot()