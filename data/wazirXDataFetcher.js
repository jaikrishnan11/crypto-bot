var request = require('request');
let _ = require("lodash")
let co = require("co");
let fs = require("fs");

function makeRequest(options) {
    return new Promise((resolve, reject) => {
        request(options, function (err, res, body) {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve(body);
        });
    })
}

var options = {
    'method': 'GET',
    'url': 'https://api.wazirx.com/api/v2/tickers/usdtinr',
    'headers': {
    }
};

co(function* () {
    while (true) {
        try {
	    let fileName = new Date().toISOString().split("T")[0] + "_ustd_inr_result"
            let result = yield makeRequest(options);
            // console.log(JSON.parse(result)["ticker"]["last"]);
            fs.appendFileSync("./" + fileName, new Date().getTime() + ";" + result + "\n")
        } catch (err) {

        }
    }
})