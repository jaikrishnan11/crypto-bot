Order response
[Object: null prototype] {
  symbol: 'ETHUSDT',
  orderId: 6918446988,
  orderListId: -1,
  clientOrderId: 'axzUeeHjV2wye6Yhb0Tyeu',
  transactTime: 1638624733425,
  price: '3930.00000000',
  origQty: '0.01000000',
  executedQty: '0.00000000',
  cummulativeQuoteQty: '0.00000000',
  status: 'NEW',
  timeInForce: 'GTC',
  type: 'LIMIT',
  side: 'BUY',
  fills: []
}

Sell order API curl request

curl 'https://www.binance.com/bapi/futures/v1/private/future/order/place-order' \
  -H 'authority: www.binance.com' \
  -H 'x-trace-id: 2f2cee10-2ef0-4166-a7ca-f186220ee48d' \
  -H 'csrftoken: 67c8a5c83b4dfbbf9dcceca9e21d6efb' \
  -H 'x-ui-request-trace: 2f2cee10-2ef0-4166-a7ca-f186220ee48d' \
  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36' \
  -H 'content-type: application/json' \
  -H 'lang: en' \
  -H 'fvideo-id: 32b356ae5a37af26bdf736c55352880627cebfb4' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"' \
  -H 'device-info: eyJzY3JlZW5fcmVzb2x1dGlvbiI6IjE3OTIsMTEyMCIsImF2YWlsYWJsZV9zY3JlZW5fcmVzb2x1dGlvbiI6IjE3OTIsMTEyMCIsInN5c3RlbV92ZXJzaW9uIjoiTWFjIE9TIDEwLjE1LjciLCJicmFuZF9tb2RlbCI6InVua25vd24iLCJzeXN0ZW1fbGFuZyI6ImVuLUdCIiwidGltZXpvbmUiOiJHTVQrNiIsInRpbWV6b25lT2Zmc2V0IjotMzMwLCJ1c2VyX2FnZW50IjoiTW96aWxsYS81LjAgKE1hY2ludG9zaDsgSW50ZWwgTWFjIE9TIFggMTBfMTVfNykgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzk2LjAuNDY2NC41NSBTYWZhcmkvNTM3LjM2IiwibGlzdF9wbHVnaW4iOiJDaHJvbWUgUERGIFBsdWdpbixDaHJvbWUgUERGIFZpZXdlcixOYXRpdmUgQ2xpZW50IiwiY2FudmFzX2NvZGUiOiJmN2E0NWVlZCIsIndlYmdsX3ZlbmRvciI6IkludGVsIEluYy4iLCJ3ZWJnbF9yZW5kZXJlciI6IkludGVsKFIpIFVIRCBHcmFwaGljcyA2MzAiLCJhdWRpbyI6IjEyNC4wNDM0NzY1NzgwODEwMyIsInBsYXRmb3JtIjoiTWFjSW50ZWwiLCJ3ZWJfdGltZXpvbmUiOiJBc2lhL0NhbGN1dHRhIiwiZGV2aWNlX25hbWUiOiJDaHJvbWUgVjk2LjAuNDY2NC41NSAoTWFjIE9TKSIsImZpbmdlcnByaW50IjoiMjM5NzcxMTU0ODI1MDgyNmI2MTAyN2Y3MzYyMDRjY2MiLCJkZXZpY2VfaWQiOiIiLCJyZWxhdGVkX2RldmljZV9pZHMiOiIifQ==' \
  -H 'bnc-uuid: cde70027-bc0f-4a8b-b4e4-189b1be1a768' \
  -H 'clienttype: web' \
  -H 'sec-ch-ua-platform: "macOS"' \
  -H 'accept: */*' \
  -H 'origin: https://www.binance.com' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://www.binance.com/en/futures/ETHUSDT' \
  -H 'accept-language: en-GB,en-US;q=0.9,en;q=0.8' \
  -H 'cookie: cid=opkRJNuX; bnc-uuid=cde70027-bc0f-4a8b-b4e4-189b1be1a768; _ga=GA1.2.51209529.1638365357; userPreferredCurrency=USD_USD; monitor-uuid=93dad64b-91f2-4b4b-8579-b5aa390f7af2; _gid=GA1.2.2039941323.1638544982; BNC_FV_KEY=32b356ae5a37af26bdf736c55352880627cebfb4; BNC_FV_KEY_EXPIRE=1638631382910; source=referral; campaign=accounts.binance.com; cr00=47A0992B005C5A89029AF06175B644CE; d1og=web.173146965.744DCBFA4623BE158CA7DDDB19A77FF2; r2o1=web.173146965.B2550B18C81AE101DC3361D5B3EBEEAA; f30l=web.173146965.133076556448CE160DABD01DD60E8801; logined=y; p20t=web.173146965.40AF1EE4644D2935BC9B0F853EE2F1C3; fiat-prefer-currency=USD; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22173146965%22%2C%22first_id%22%3A%2217d762f62f57c8-07f88f0d19846c-1f396452-2007040-17d762f62f6b1c%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%2217d762f62f57c8-07f88f0d19846c-1f396452-2007040-17d762f62f6b1c%22%7D; home-ui-ab=B; __ssid=880bf3c00af5e9835a4e3c85d44e559; rskxRunCookie=0; rCookie=mqh1h79okt8zv1l3mhse3mkwqr4bnc; lastRskxRun=1638558010385; lang=en; futures-layout=pro; _gat_UA-162512367-1=1' \
  --data-raw '{"symbol":"ETHUSDT","type":"MARKET","side":"SELL","positionSide":"BOTH","quantity":0.002,"reduceOnly":false,"placeType":"order-form"}' \
  --compressed



Buy order

curl 'https://www.binance.com/bapi/futures/v1/private/future/order/place-order' \
  -H 'authority: www.binance.com' \
  -H 'x-trace-id: c5415ce5-a997-49b8-91e8-90de36870797' \
  -H 'csrftoken: 67c8a5c83b4dfbbf9dcceca9e21d6efb' \
  -H 'x-ui-request-trace: c5415ce5-a997-49b8-91e8-90de36870797' \
  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36' \
  -H 'content-type: application/json' \
  -H 'lang: en' \
  -H 'fvideo-id: 32b356ae5a37af26bdf736c55352880627cebfb4' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"' \
  -H 'device-info: eyJzY3JlZW5fcmVzb2x1dGlvbiI6IjE3OTIsMTEyMCIsImF2YWlsYWJsZV9zY3JlZW5fcmVzb2x1dGlvbiI6IjE3OTIsMTEyMCIsInN5c3RlbV92ZXJzaW9uIjoiTWFjIE9TIDEwLjE1LjciLCJicmFuZF9tb2RlbCI6InVua25vd24iLCJzeXN0ZW1fbGFuZyI6ImVuLUdCIiwidGltZXpvbmUiOiJHTVQrNiIsInRpbWV6b25lT2Zmc2V0IjotMzMwLCJ1c2VyX2FnZW50IjoiTW96aWxsYS81LjAgKE1hY2ludG9zaDsgSW50ZWwgTWFjIE9TIFggMTBfMTVfNykgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzk2LjAuNDY2NC41NSBTYWZhcmkvNTM3LjM2IiwibGlzdF9wbHVnaW4iOiJDaHJvbWUgUERGIFBsdWdpbixDaHJvbWUgUERGIFZpZXdlcixOYXRpdmUgQ2xpZW50IiwiY2FudmFzX2NvZGUiOiJmN2E0NWVlZCIsIndlYmdsX3ZlbmRvciI6IkludGVsIEluYy4iLCJ3ZWJnbF9yZW5kZXJlciI6IkludGVsKFIpIFVIRCBHcmFwaGljcyA2MzAiLCJhdWRpbyI6IjEyNC4wNDM0NzY1NzgwODEwMyIsInBsYXRmb3JtIjoiTWFjSW50ZWwiLCJ3ZWJfdGltZXpvbmUiOiJBc2lhL0NhbGN1dHRhIiwiZGV2aWNlX25hbWUiOiJDaHJvbWUgVjk2LjAuNDY2NC41NSAoTWFjIE9TKSIsImZpbmdlcnByaW50IjoiMjM5NzcxMTU0ODI1MDgyNmI2MTAyN2Y3MzYyMDRjY2MiLCJkZXZpY2VfaWQiOiIiLCJyZWxhdGVkX2RldmljZV9pZHMiOiIifQ==' \
  -H 'bnc-uuid: cde70027-bc0f-4a8b-b4e4-189b1be1a768' \
  -H 'clienttype: web' \
  -H 'sec-ch-ua-platform: "macOS"' \
  -H 'accept: */*' \
  -H 'origin: https://www.binance.com' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://www.binance.com/en/futures/ETHUSDT' \
  -H 'accept-language: en-GB,en-US;q=0.9,en;q=0.8' \
  -H 'cookie: cid=opkRJNuX; bnc-uuid=cde70027-bc0f-4a8b-b4e4-189b1be1a768; _ga=GA1.2.51209529.1638365357; userPreferredCurrency=USD_USD; monitor-uuid=93dad64b-91f2-4b4b-8579-b5aa390f7af2; _gid=GA1.2.2039941323.1638544982; BNC_FV_KEY=32b356ae5a37af26bdf736c55352880627cebfb4; BNC_FV_KEY_EXPIRE=1638631382910; source=referral; campaign=accounts.binance.com; cr00=47A0992B005C5A89029AF06175B644CE; d1og=web.173146965.744DCBFA4623BE158CA7DDDB19A77FF2; r2o1=web.173146965.B2550B18C81AE101DC3361D5B3EBEEAA; f30l=web.173146965.133076556448CE160DABD01DD60E8801; logined=y; p20t=web.173146965.40AF1EE4644D2935BC9B0F853EE2F1C3; fiat-prefer-currency=USD; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22173146965%22%2C%22first_id%22%3A%2217d762f62f57c8-07f88f0d19846c-1f396452-2007040-17d762f62f6b1c%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%2217d762f62f57c8-07f88f0d19846c-1f396452-2007040-17d762f62f6b1c%22%7D; home-ui-ab=B; __ssid=880bf3c00af5e9835a4e3c85d44e559; rskxRunCookie=0; rCookie=mqh1h79okt8zv1l3mhse3mkwqr4bnc; lastRskxRun=1638558010385; lang=en; futures-layout=pro; _gat_UA-162512367-1=1' \
  --data-raw '{"symbol":"ETHUSDT","type":"MARKET","side":"BUY","positionSide":"BOTH","quantity":0.002,"reduceOnly":true,"placeType":"order-form"}' \
  --compressed


    clientOrderId: 'RUHzelt7ASv4VECWjUz0Uq',