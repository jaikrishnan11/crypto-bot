const EventEmitter = require('events')
const fs = require('fs')

class AbstractExchange extends EventEmitter {
    constructor(opts) {
        super()
        this.options = opts
        this.unFulfilledBuyOrderIds = new Set();
        this.unFulfilledSellOrderIds = new Set();
        this.restoreState()
    }

    placeOrder(type, order) { throw 'Not Implemented'; }

    restoreState() {
        if(this.restoreState && fs.existsSync(this.options?.stateFilePath)){
            let state  = JSON.parse(fs.readFileSync(this.options.stateFilePath));
            this.unFulfilledBuyOrderIds = new Set(state.unFulfilledBuyOrderIds)
            this.unFulfilledSellOrderIds = new Set(state.unFulfilledSellOrderIds)
        }
    }

    saveState() {
        let state = {unFulfilledBuyOrderIds: Array.from(this.unFulfilledBuyOrderIds), unFulfilledSellOrderIds: Array.from(this.unFulfilledSellOrderIds)}
        fs.writeFileSync(this.options.stateFilePath, JSON.stringify(state))
    }
}

module.exports = AbstractExchange