const crypto = require('crypto')
const request = require('request')
const logger = require('winston')
const _ = require('lodash')
const util = require('util')

const CONFIG = require('config').get('coindcxExchange');
const AbstractExchange = require('./AbstractExchange')
const Events = require('./Events')
const coinDCXProfile = require('config').get('coindcxProfiles')[CONFIG.profile]
const telegramBot = require('../clients/telegram-client')

const defaultOptions = {
    dryRun: false
}

/*
    1. INR cannot be treated as a commodity
    2. USDT precision should be 2
    3. Max 25 standing open orders for a given pair
*/

class CoinDCX extends AbstractExchange {
    constructor(opts) {
        super(Object.assign({ stateFilePath: CONFIG.stateFilePath, restoreState: require('config').get('restoreState') }, defaultOptions, opts))
        this.runPoller = CONFIG.runPoller;
        logger.info(`Start poller : ${this.runPoller}`)
        // this.getTicker()
    }

    async startOrderStatusPoller() {
        if(this.runPoller){
            await this.pollOrderStatus()
            setTimeout(this.startOrderStatusPoller.bind(this), CONFIG.ORDER_STATUS_CHECK_DELAY * 1000)
        }
    }

    async getTicker() {
        let ticker = {};
        let failCount = 0;
        do {
            await new Promise(res => setTimeout(res, CONFIG.TICKER_DELAY_AFTER_FIRST_TRY * 1000))
            const response = await util.promisify(request.get)(CONFIG.baseURL + "/exchange/ticker");
            if (response.statusCode >= 400) {
                logger.error(`CoinCDX getTicker failed (${++failCount} times). Sleeping..`, { body: response.body, statusCode: response.statusCode })
                await new Promise(res => setTimeout(res, Math.max(failCount, 15) * 1000))
                continue;
            }
            const tickers = JSON.parse(response.body);
            ticker = tickers.find(t => t.market == "USDTINR")
        } while (this.lastTicketTimeStamp == ticker.timestamp)
        this.lastTicketTimeStamp = ticker.timestamp
        // {"market":"USDTINR","change_24_hour":"0.09","high":"77.35","low":"76.33","volume":"131991781.922","last_price":"77.250000","bid":"77.250000","ask":"77.270000","timestamp":1630178957}
        this.emit(Events.TICK, this.convertToCamelCase(ticker));
        setTimeout(this.getTicker.bind(this), CONFIG.NEXT_TICK_DELAY)
    }

    async getCurTicker(pair = "USDTINR") {
        const response = await util.promisify(request.get)(CONFIG.baseURL + "/exchange/ticker");
        if (response.statusCode >= 400) {
            logger.error(`CoinCDX getCurTicker failed`, { body: response.body, statusCode: response.statusCode })
            await new Promise(res => setTimeout(res, 250))
            return this.getCurTicker();
        }
        const tickers = JSON.parse(response.body)
        let ticker = tickers.find(t => t.market == pair)
        return this.convertToCamelCase(ticker);
    }

    async getBalance({ retryCount = 0 } = {}){
        const body = { "timestamp": Math.floor(Date.now())  } 
        const options = {
            url: CONFIG.baseURL + "/exchange/v1/users/balances",
            headers: {
                'X-AUTH-APIKEY': coinDCXProfile.key,
                'X-AUTH-SIGNATURE': this.signPayload(body)
            },
            json: true,
            body
        }
        const response = await util.promisify(request.post)(options)
        if(response.statusCode >= 400 || !Array.isArray(response.body)){
            logger.error(`Error retreiving balance.`, {retryCount, response})
            if(retryCount > 5)
                throw `getBalance failed ${response.body}`
            await new Promise(res => setTimeout(res, 2000))
            return this.getBalance({retryCount: retryCount + 1})
        }
        let balanceMap = {}
        response.body.map(balance => this.convertToCamelCase(balance))
                            .map(balance => {
                                return {
                                    ...balance,
                                    balance: parseFloat(balance.balance),
                                    lockedBalance: parseFloat(balance.lockedBalance)
                                }
                            })
                            .forEach(balance => {
                                balanceMap[balance.currency] = balance
                            })
        return balanceMap
    }

    async pollOrderStatus() {
        if (this.runPoller == false) {
            logger.info("Stopping order status poller")
            return
        }
        try {
            let ids = [...this.unFulfilledBuyOrderIds, ...this.unFulfilledSellOrderIds]

            if (ids.length != 0) {
                let orderStatuses = await this.getOrderStatus({ ids })
                orderStatuses.forEach(updatedStatus => {

                    const orderId = updatedStatus.id
                    if (updatedStatus.status == "filled") {
                        const event = this.unFulfilledSellOrderIds.has(orderId) ? Events.SELL_ORDER_FILLED : Events.BUY_ORDER_FILLED
                        this.unFulfilledSellOrderIds.delete(orderId)
                        this.unFulfilledBuyOrderIds.delete(orderId)
                        this.emit(event, { id: orderId, ...updatedStatus })
                    } else if(updatedStatus.status == "partially_filled") {
                        const event = updatedStatus.side == "sell" ? Events.SELL_ORDER_PARTIALLY_FILLED : Events.BUY_ORDER_PARTIALLY_FILLED
                        this.emit(event, {id: orderId, ...updatedStatus})
                    } else if (updatedStatus.status == "cancelled") {
                        logger.warn(`Order ${orderId} cancelled. Cancelled from UI ?`)
                        // BUG ..... should not resolve .
                        this.unFulfilledSellOrderIds.delete(orderId)
                        this.unFulfilledBuyOrderIds.delete(orderId)
                    }
                })
            }
        } catch (e) {
            logger.error(`Error getting order status`, e)
            // This is going to spam group
            await telegramBot.sendErrorMessage(`Poll order status failed : ${e.message}`)
            throw e;
        }
    }

    async getOrderStatus({ ids = [], retryCount = 0 }) {
        if (ids.length == 0)
            return []

        const orderStatusRequestBody = {
            "ids": ids,
            "timestamp": Math.floor(Date.now())
        }

        const options = {
            url: CONFIG.baseURL + "/exchange/v1/orders/status_multiple",
            headers: {
                'X-AUTH-APIKEY': coinDCXProfile.key,
                'X-AUTH-SIGNATURE': this.signPayload(orderStatusRequestBody)
            },
            json: true,
            body: orderStatusRequestBody
        }

        const response = await util.promisify(request.post)(options)
        if (response.statusCode >= 400 || response.body == undefined || Array.isArray(response.body) == false) {
            logger.error(`pollOrder Status`, { response, statusCode: response.statusCode })
            await telegramBot.sendErrorMessage(`Poll order status failed(code : ${response.statusCode}). Retrying...`, { body: response.body })
            if (retryCount > 10) {
                throw response
            }
            await new Promise(res => setTimeout(res, 1000))
            return this.getOrderStatus({ ids, retryCount: ++retryCount });
        }
        return response.body.map(this.convertToCamelCase)
    }

    async placeOrder(type, requestBody) {
        let body = {
            "side": type,  //Toggle between 'buy' or 'sell'.
            "order_type": requestBody.orderType || "limit_order", //Toggle between a 'market_order' or 'limit_order'.
            "market": requestBody.market || "USDTINR",
            "price_per_unit": requestBody.limit, //This parameter is only required for a 'limit_order'
            "total_quantity": requestBody.quantity, //Replace this with the quantity you want
            "timestamp": Math.floor(Date.now())
        }
        let order = await this._postOrder(body);
        (type == 'buy' ? this.unFulfilledBuyOrderIds : this.unFulfilledSellOrderIds).add(order.id)
        return order
    }

    async cancelOrders({ ids, stopRetry = false }) {
        if (ids.length == 0)
            return
        let body = {
            "ids": ids
        }
        const options = {
            url: CONFIG.baseURL + "/exchange/v1/orders/cancel_by_ids",
            headers: {
                'X-AUTH-APIKEY': coinDCXProfile.key,
                'X-AUTH-SIGNATURE': this.signPayload(body)
            },
            json: true,
            body: body
        };
        let response = await util.promisify(request.post)(options);
        if (response.statusCode >= 400) {
            logger.error(`CoinCDX failed to cancel orders`, { response, ids, statusCode: response.statusCode })
            await telegramBot.sendErrorMessage(`Failed to cancel orders. System in inconsistent state!! (code : ${response.statusCode})`, { body: response.body })
            if (stopRetry) {
                await new Promise(resolve => setTimeout(resolve, 1000))
                return this.cancelOrders({ ids, stopRetry: false })
            }
            return
        }
        ids.forEach(id => {
            this.unFulfilledBuyOrderIds.delete(id)
        })
        //Do we need to delete sell orders ?
        ids.forEach(id => this.unFulfilledSellOrderIds.delete(id))
    }

    async _postOrder(body, retryCount = 1) {
        const options = {
            url: CONFIG.baseURL + "/exchange/v1/orders/create",
            headers: {
                'X-AUTH-APIKEY': coinDCXProfile.key,
                'X-AUTH-SIGNATURE': this.signPayload(body)
            },
            json: true,
            body: body
        };

        let response, orderDetails
        if (!this.options.dryRun) {
            response = await util.promisify(request.post)(options);
            if(response.statusCode == 429 && retryCount <= 10){
                logger.error(`Getting throttled(${response.statusCode})`, {response, retryCount});
                await new Promise(res => setTimeout(res, Math.min(10000, CONFIG.waitTime429 * retryCount)))
                return await this._postOrder(body, retryCount++)
            }
            else if (response.statusCode >= 400) {
                logger.error(`CoinCDX  ${body.side} order failed`, { response: response.body, requestBody: body, statusCode: response.statusCode, headers: response.headers })
                throw response.body;
            }
            orderDetails = response.body.orders[0];
        }
        else {
            logger.debug(`CoinDCX dry run  ${body.side} order.`)
            orderDetails = { id: "RandomOrderId-" + Math.random(), status: "filled", totalQuantity: body.total_quantity }
        }
        logger.info(`CoinDCX ${body.side} order submitted`, { limit: body.price_per_unit, quantity: body.total_quantity, orderId: orderDetails.id });
        return this.convertToCamelCase(orderDetails)
    }

    signPayload(body) {
        const payload = Buffer.from(JSON.stringify(body)).toString();
        const signature = crypto.createHmac('sha256', coinDCXProfile.secret).update(payload).digest('hex');
        return signature
    }

    convertToCamelCase(obj) {
        return _.mapKeys(obj, (v, k) => _.camelCase(k))
    }

    async getActiveOrders(pair = "USDTINR") {
        let body = {
            "market": pair,
            "timestamp": Math.floor(Date.now())
        }
        const options = {
            url: CONFIG.baseURL + "/exchange/v1/orders/active_orders",
            headers: {
                'X-AUTH-APIKEY': coinDCXProfile.key,
                'X-AUTH-SIGNATURE': this.signPayload(body)
            },
            json: true,
            body: body
        }

        let response = await util.promisify(request.post)(options);
        if (response?.body?.orders == undefined || response.statusCode >= 400) {
            logger.error(`Error retriving active orders`, { response, body })
            throw response
        }
        return response.body.orders.map(order => this.convertToCamelCase(order))
    }
}

module.exports = CoinDCX;