const readline = require('readline');
const fs = require('fs');
const logger = require('winston');
const request = require('request');
const AbstractExchange = require('./AbstractExchange')
const Events = require('./Events')
const Config = require('config').get("testExchange")

class TestExchange extends AbstractExchange{
    constructor() {
        super({stateFilePath: Config.stateFilePath, restoreState: require('config').get('restoreState')})
        var data = fs.readFileSync(Config.backTestDataPath, 'utf-8');
        this.tickers = [];
        this.placedOrders = new Set()
        this.counter = 0;
        data.split("\n").forEach(line => {
            this.tickers.push(line.split(","))
        });
        setTimeout(this.getTicker.bind(this), 2)
    }

    async getTicker() {
        for( ; this.counter < this.tickers.length; this.counter++){
            let curPrice = parseFloat(this.tickers[this.counter][1])
           Array.from(this.placedOrders).forEach(order => {
               if(order.isBuy && order.limit >= curPrice){
                this.emit(Events.BUY_ORDER_FILLED, {id: order.id, ...order, status: Events.OrderStatus.FILLED, feeAmount: 0.01})
                this.placedOrders.delete(order)
               }
               else if(order.isBuy == false && order.limit <= curPrice){
                this.emit(Events.SELL_ORDER_FILLED, {id: order.id, ...order, status: Events.OrderStatus.FILLED, feeAmount: 0.1})
                this.placedOrders.delete(order)   
               }
           })
           //await Promise.resolve()
           await new Promise(res => setTimeout(res, 1))
        }
    }

    async startOrderStatusPoller() {

    }
    async getCurTicker(pair = "USDTINR"){
        let curPrice = parseFloat(this.tickers[this.counter][1])
        return {"market":pair,"change_24_hour":"0.09","high":"77.35","low":"76.33","volume":"131991781.922","lastPrice":this.tickers[this.counter++][1],"bid":"77.250000","ask":"77.270000","timestamp":1630178957}
    }

    async placeOrder(type, requestBody) {
        
        let order =  {
            id: `OrderId-${Math.random()}`,
            status: Events.OrderStatus.OPEN, 
            totalQuantity: requestBody.quantity,
            pricePerUnit: requestBody.limit,
            avgPrice: requestBody.limit,
            limit: requestBody.limit,
            ...requestBody
        }
        logger.info(`${type} order placed`, {...requestBody, ...order})
        this.placedOrders.add({...order, totalQuantity: requestBody.quantity, id: order.id, isBuy: type=='buy', limit: requestBody.limit, pricePerUnit: requestBody.limit})
        //await new Promise(res => setTimeout(res, 1))

        return {...requestBody, ...order, feeAmount: 0.01}
    }

    async cancelOrders(...ids) {
        this.placedOrders.forEach(order =>{
            if(ids.indexOf(order.id) != -1){
                this.placedOrders.delete(order)
            }
        })
    }

}

module.exports = TestExchange