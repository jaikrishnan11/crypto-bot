const Binance = require('node-binance-api');
const _ = require("lodash");
const CONFIG = require('config').get('binanceExchange');
const PROFILES = require('config').get('binanceProfiles');
const { wait, retryOnException } = require('../strategy/utils')
const logger = require("winston")
const EventEmitter = require('events')
const exactMath = require('exact-math')

class BinanceClient extends EventEmitter {
    constructor({ symbol, profile }) {
        super()
        const profileData = PROFILES.get(profile) || PROFILES.get(CONFIG.profile)
        this.binance = new Binance(CONFIG.constructor).options({
            APIKEY: profileData.apiKey,
            APISECRET: profileData.apiSecret,
            log: logger.info, // TODO : Create a seperate logger
            ...CONFIG.constructor
        });
        this.symbol = symbol;
        this.initDone = false

    }

    async init() {
        if (this.initDone)
            return;
        if (CONFIG.cancelOpenOrders) {
            logger.info(`Cancelling open orders`)
            let cancelledOrders = await this.cancelOpenOrders()
            logger.debug(`Cancelled Orders before starting`, { cancelledOrders })
        }
        await this.binance.futuresMarginType(this.symbol, 'CROSSED');
        await this.binance.futuresLeverage(this.symbol, 15);

        this.binance.websockets.userFutureData(e => {
            console.log("What am i supposed to do with this")
            console.log(e)
        }, null, orderUpdateEvent => {
            /*
            NEW
            PARTIALLY_FILLED
            FILLED
            CANCELED
            REJECTED
            EXPIRED
            */
            let order = orderUpdateEvent.order
            if (order.orderStatus == 'FILLED') {// Handle other status - cancel ... 
                this.emit(order.side == 'BUY' ? 'BUY_FILLED' : 'SELL_FILLED', order)
            }
        })
        await wait(3000)
        this.initDone = true;
    }

    async getAccountDetails() {
        return await this.binance.futuresAccount();
    }

    async getOpenOrders() {
        return await this.binance.openOrders(this.symbol);
    }

    async placeMarketSellFutures(quantity) {
        return await this.binance.futuresMarketSell(this.symbol, quantity)
    }

    async placeMarketBuyFutures(quantity) {
        return await this.binance.futuresMarketBuy(this.symbol, quantity, {
            reduceOnly: true,
            timeInForce: "GTC"
        })
    }

    async futureMarkPrice(symbol) {
        return await this.binance.futuresMarkPrice(symbol)
    }

    //Futures
    async placeTrailingBuyOrder(quantity, callbackRate, activationPrice) { //expecting in string values except quantity
        return await this.binance.futuresBuy(this.symbol, quantity, activationPrice, { 
            "symbol": this.symbol, 
            "type": "TRAILING_STOP_MARKET", 
            "side": "BUY", 
            "positionSide": "BOTH", 
            "quantity": quantity, 
            "reduceOnly": false, 
            "activationPrice": activationPrice, 
            "callbackRate" : callbackRate,
            "workingType": "MARK_PRICE", 
            "priceProtect": true
        })
    }

    // async placeTrailingSellOrder(quantity, callbackRate, activationPrice) { //expecting in string values except quantity
    //     return await this.binance.futuresBuy(this.symbol, quantity, activationPrice, { 
    //         "symbol": this.symbol, 
    //         "type": "TRAILING_STOP_MARKET", 
    //         "side": "SELL", 
    //         "positionSide": "BOTH", 
    //         "quantity": quantity, 
    //         "reduceOnly": false, 
    //         "activationPrice": activationPrice, 
    //         "callbackRate" : callbackRate,
    //         "workingType": "MARK_PRICE", 
    //         "priceProtect": true
    //     })
    // }

    async getShortContract(quantity, price) {
        quantity = exactMath.round(quantity, -3)
        price = exactMath.round(price, -2)
        return await this.binance.futuresSell(this.symbol, quantity, price, {
            timeInForce: "GTC"
            //  type: "STOP",
            //  stopPrice: price + 1
        })
    }

    async releaseShortContract(quantity, price) {
        if (quantity <= 0) {
            logger.info(`releaseLongContract quantity zero. NoOp`)
            return { code: 200, message: "Qty zero" };
        }
        quantity = exactMath.round(quantity, -3)
        if (price) {
            price = exactMath.round(price, -2)
            return await this.binance.futuresBuy(this.symbol, quantity, price, {
                reduceOnly: true,
                timeInForce: "GTC"
            })
        } else {
            return await this.binance.futuresMarketBuy(this.symbol, quantity, {
                reduceOnly: true
            });
        }
    }

    async getLongContract(quantity, price, params = {}) {
        quantity = exactMath.round(quantity, -3)
        price = exactMath.round(price, -2)
        return await this.binance.futuresBuy(this.symbol, quantity, price, Object.assign({
            timeInForce: "GTC"
        }, params))
    }

    async getMarketLongContract(quantity) {
        return await this.binance.futuresMarketBuy(this.symbol, quantity)
    }

    async releaseLongContract(quantity, price) {
        if (quantity <= 0) {
            logger.info(`releaseLongContract quantity zero. NoOp`)
            return { code: 200, message: "Qty zero" };
        }
        quantity = exactMath.round(quantity, -3)
        if (price) {
            price = exactMath.round(price, -2)
            return await this.binance.futuresSell(this.symbol, quantity, price, {
                reduceOnly: true,
                timeInForce: "GTC"
            })
        }
        else {
            return await this.binance.futuresMarketSell(this.symbol, quantity, {
                reduceOnly: true
            })
        }
    }

    async getFuturesBalance() {
        let balances = await this.binance.futuresBalance();
        let usdtBalance = _.filter(balances, (balance) => balance["asset"] == "USDT");
        return !_.isEmpty(usdtBalance) && !_.isEmpty(usdtBalance[0]) && usdtBalance[0].balance;
    }

    async cancelOrderId(orderId) {
        return await this.binance.futuresCancel(this.symbol, {orderId: orderId})
    }
    async futuresOrderStatus(params) { //params: { origClientOrderId: 'eSDdUIFlxGfgN62zc3m4Fo' }
        return await this.binance.futuresOrderStatus(this.symbol, params);
    }

    async futuresPrices(params) {
        const futuresPrices =  await this.binance.futuresPrices()
        return parseFloat(futuresPrices[this.symbol])
    }
    //Normal trade
    async getBalances() {
        return await this.binance.balance();
    }

    async orderStatus(orderId) {
        try {
            //recvWindow -> window of the order placed, old order won't come if the window is small
            return await this.binance.orderStatus(this.symbol, undefined, undefined, { recvWindow: 30000, origClientOrderId: orderId })
        } catch (err) {
            console.log("[orderStatus]Error in getting order status", orderId, err);
            return err;
        }
    }

    async placeLimitBuy(quantity, price) {
        try {
            return await this.binance.buy(this.symbol, quantity, price);
        } catch (err) {
            console.log("[placeLimitBuy]Error in placing limit buy", quantity, price);
            return err;
        }
    }

    async placeLimitSell(quantity, price) {
        try {
            return await this.binance.sell(this.symbol, quantity, price);
        } catch (err) {
            console.log("[placeLimitSell]Error in placing limit sell", quantity, price);
            return err;
        }
    }

    async cancelOpenOrders() {
        try {
            return await this.binance.futuresCancelAll(this.symbol)
        } catch (err) {
            logger.error(`cancelOpenOrders`, { err })
            throw err;
        }
    }

    async futuresUserTrades(params = {}) {
        return await this.binance.futuresIncome(params)
    }

}


const handleBinanceExceptions = function (result) {

    if (this.binance.statusCode() >= 400) {
        throw { ...result, statusCode: this.binance.statusCode() }
    }
    else if (!result.code || result.code < 400)
        return result;
    throw result
}

BinanceClient.prototype.getLongContract = retryOnException(BinanceClient.prototype.getLongContract, 500, { resultProcesser: handleBinanceExceptions })
BinanceClient.prototype.getShortContract = retryOnException(BinanceClient.prototype.getShortContract, 500, { resultProcesser: handleBinanceExceptions })

BinanceClient.prototype.releaseLongContract = retryOnException(BinanceClient.prototype.releaseLongContract, 500, { resultProcesser: handleBinanceExceptions })
BinanceClient.prototype.releaseShortContract = retryOnException(BinanceClient.prototype.releaseShortContract, 500, { resultProcesser: handleBinanceExceptions })

BinanceClient.prototype.futureMarkPrice = retryOnException(BinanceClient.prototype.futureMarkPrice, 500, { resultProcesser: handleBinanceExceptions })
BinanceClient.prototype.cancelOpenOrders = retryOnException(BinanceClient.prototype.cancelOpenOrders, 500, { resultProcesser: handleBinanceExceptions })
BinanceClient.prototype.futuresUserTrades = retryOnException(BinanceClient.prototype.futuresUserTrades, 500, { resultProcesser: handleBinanceExceptions })


module.exports = BinanceClient